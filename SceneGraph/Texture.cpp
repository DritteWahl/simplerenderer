#include "Texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "Tools/stb_image.h"
#include <iostream>
#include "Tools/Helpers.hpp"


std::string toString(TextureType textureType) {
	switch (textureType) {
	case DiffuseMap: return "diffuseMap";
	case NormalMap: return "normalMap";
	case SpecularMap: return "specularMap";
	case OpacityMap: return "opacityMap";
	case DepthMap: return "depthMap";
	}
};


Texture::Texture() : Texture(DiffuseMap) {

}

Texture::Texture(TextureType _textureType) {
	setTextureType(_textureType);

	wrappingModeS = GL_REPEAT;
	wrappingModeT = GL_REPEAT;
	minFiltering = GL_LINEAR;
	magFiltering = GL_LINEAR;
	texFormat = GL_RGB;

	glGenTextures(1, &texId);
}

Texture::~Texture() {
	glDeleteTextures(1, &texId);
}

void Texture::create() {
	int width, height, channelsCount;
	unsigned char *data = stbi_load(path.c_str(), &width, &height, &channelsCount, STBI_rgb);
	

	if (data) {
		GLenum channels = (channelsCount == 4) ? GL_RGBA : GL_RGB;
		glBindTexture(GL_TEXTURE_2D, texId);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, channels, width, height, 0, channels, GL_UNSIGNED_BYTE, data);
		stbi_image_free(data);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else {
		// TODO better exception handling
		std::cout << "WARNING: Failed to load texture " << path << "!" << std::endl;
	}

	helpers::checkGLError();
}

void Texture::bind(ShaderProgram* program, int texTarget) {
	glActiveTexture(GL_TEXTURE0 + texTarget);
	glBindTexture(GL_TEXTURE_2D, texId);

	program->setIntUniform(toString(textureType), texTarget);
	program->setIntUniform(textureTypeBool, true);

	// TODO mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrappingModeS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrappingModeT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFiltering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFiltering);

	glActiveTexture(GL_TEXTURE0);

	helpers::checkGLError();
}

void Texture::release(ShaderProgram* program, int texTarget) {
	program->setIntUniform(textureTypeBool, false);
	program->setIntUniform(toString(textureType), 0);

	helpers::checkGLError();
}

void Texture::setTextureImage(std::string newPath) {
	path = newPath;
}

void Texture::setWrappingMode(GLenum newWrappingModeS, GLenum newWrappingModeT) {
	// TODO catch wrong input for all getters
	wrappingModeS = newWrappingModeS;
	wrappingModeT = newWrappingModeT;
}

void Texture::setFiltering(GLenum newMinFiltering, GLenum newMagFiltering) {
	minFiltering = newMinFiltering;
	magFiltering = newMagFiltering;
}

void Texture::setTexFormat(GLenum newTexFormat) {
	texFormat = newTexFormat;
	create();
}

void Texture::setTextureType(TextureType newTextureType) {
	textureType = newTextureType;

	textureTypeBool = "has";
	textureTypeBool += (char)toupper(toString(textureType)[0]);
	textureTypeBool.append(toString(textureType).substr(1));
}
