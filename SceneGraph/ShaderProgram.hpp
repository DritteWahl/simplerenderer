#pragma once

#include <string>
#include <gl/glew.h>
#include <glm/glm.hpp>
#include <vector>

class ShaderProgram {
	
public:
	ShaderProgram();
	~ShaderProgram();

	void create();
	void bind();
	void release();
	void bindMVP(glm::mat4 M, glm::mat4 V, glm::mat4 P);
	void setSeperatedMVP(bool seperatedMVP);
	void setShaders(std::string vShaderPath, std::string fShaderPath);
	GLint getProgram();
	void setIntUniform(std::string name, int value);

private:
	GLuint loadShader(std::string shaderPath, GLenum shaderType);
	void cleanup();

	GLuint program;
	GLuint vShader;
	GLuint fShader;
	std::string vShaderFile = "";
	std::string fShaderFile = "";
	bool hasSeperatedMVP;

};
