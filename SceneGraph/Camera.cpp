#include "Camera.hpp"

#include "Node.hpp"

Camera::Camera() : SceneObject() {
	projection = glm::mat4(1.0);
}

Camera::~Camera() {
	
}

glm::mat4 Camera::getViewMatrix() {
	return transform->global;
}

glm::mat4 Camera::getProjectionMatrix() {
	return projection;
}

void Camera::setPerspectiveProjection(bool newIsPerspective) {
	isPerspective = newIsPerspective;
	updateProjection();
}

void Camera::toggleProjectionMode() {
	setPerspectiveProjection(!isPerspective);
}

void Camera::setZNear(float newZNear) {
	zNear = newZNear;
	updateProjection();
}

void Camera::setZFar(float newZFar) {
	zFar = newZFar;
	updateProjection();
}

void Camera::setFov(float newFov) {
	fov = newFov;
	updateProjection();
}

void Camera::setAspectRatio(float newWidth, float newHeight) {
	width = newWidth;
	height = newHeight;
	updateProjection();
}

void Camera::translate(float x, float y, float z) {
	transform->translate(-x, -y, -z);
}

void Camera::translateLocal(float x, float y, float z) {
	transform->translateLocal(-x, -y, -z);
}

void Camera::rotateAroundAxis(float angle, glm::vec3 dir) {
	transform->rotateAroundAxis(-angle, dir);
}

void Camera::rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center) {
	transform->rotateAroundPoint(angle, dir, center);
}

void Camera::setEulerAngles(float roll, float pitch, float yaw) {
	transform->setEulerAngles(-roll, -pitch, -yaw);
}

void Camera::scale(float) {

}

void Camera::scale(float, float, float) {

}

void Camera::toggleDemoMode() {
	demoMode = !demoMode;
}

void Camera::setDemoSpeed(float speed) {
	demoSpeed = speed;
}

glm::mat4 Camera::update(glm::mat4 M) {
	if (demoMode) {
		rotateAroundPoint(demoSpeed * PI, glm::vec3(0, 1, 0), glm::vec3(0));
	}

	return SceneObject::update(M);
}

void Camera::updateProjection() {
	if (isPerspective) {
		projection = glm::perspective(fov, (8.0f / 6.0f), zNear, zFar);
	}
	else {
		projection = glm::ortho(-(width / 2.0f), (width / 2.0f), -(height / 2.0f), (height / 2.0f), zNear, zFar);
	}
}