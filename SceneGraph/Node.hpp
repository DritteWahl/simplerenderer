#pragma once

#include "Tools/MathUtil.hpp"
#include "../Window/RenderContext.hpp"
#include "Transform.hpp"

class Node {

public:
	Node(uint id);
	Node(uint id, Node* parent);
	~Node() { }

	uint getId();
	void setSceneObject(SceneObject* newSceneObject);
	void addChild(Node* newNode);
	Node* getChild(uint searchId);
	void changeParent(Node* newParent);
	void remove();

	void update(glm::mat4 M);
	void draw(RenderContext* context);
	void reloadShaders();

	std::string toString();

protected:
	void setParent(Node* newParent);
	void removeChild(uint childId);

	uint id;
	std::vector<std::unique_ptr<Node>> children;
	Node* parent = nullptr;
	std::unique_ptr<SceneObject> sceneObject = nullptr;

};
