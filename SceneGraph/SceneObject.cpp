#include "SceneObject.hpp"

#include "Node.hpp"
#include "Window/RenderContext.hpp"

SceneObject::SceneObject() {
	transform = std::unique_ptr<Transform>(new Transform());
}

SceneObject::SceneObject(glm::mat4 transformMat) {
	transform = std::unique_ptr<Transform>(new Transform(transformMat));
}

SceneObject::~SceneObject() {
	
}

Node* SceneObject::getNode() {
	return node;
}

void SceneObject::setNode(Node* newNode) {
	node = newNode;
}

glm::mat4 SceneObject::update(glm::mat4 M) {
	transform->updateGlobal(M);
	glm::mat4 newM = transform->global;
	return newM;
}


void SceneObject::draw(RenderContext*) {
	return;
}

void SceneObject::reloadShaders() {
	
}

glm::mat4 SceneObject::getLocalTransform() {
	return transform->local;
}

glm::mat4 SceneObject::getGlobalTransform() {
	return transform->global;
}

void SceneObject::translate(float x, float y, float z) {
	transform->translate(x, y, z);
}

void SceneObject::translateLocal(float x, float y, float z) {
	transform->translateLocal(x, y, z);
}

void SceneObject::rotate(float xAngle, float yAngle, float zAngle) {
	transform->rotate(xAngle, yAngle, zAngle);
}

void SceneObject::rotateLocal(float xAngle, float yAngle, float zAngle) {
	transform->rotateLocal(xAngle, yAngle, zAngle);
}

void SceneObject::rotateAroundAxis(float angle, glm::vec3 dir) {
	transform->rotateAroundAxis(angle, dir);
}

void SceneObject::rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center) {
	transform->rotateAroundPoint(angle, dir, center);
}

void SceneObject::setEulerAngles(float roll, float pitch, float yaw) {
	transform->setEulerAngles(roll, pitch, yaw);
}

void SceneObject::scale(float factor) {
	transform->scale(factor);
}

void SceneObject::scale(float x, float y, float z) {
	transform->scale(x, y, z);
}