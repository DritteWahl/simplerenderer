#pragma once

#include "LightSource.hpp"

class PointLight : public LightSource {
	
public:
	PointLight();
	~PointLight();

	void setAttenuation(float constant, float linear, float quadratic);
	glm::vec3 getAttenuation();

protected:
	glm::vec3 attenuation;

};
