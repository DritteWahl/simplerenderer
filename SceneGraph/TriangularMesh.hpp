#pragma once

#include "Geometry.hpp"
#include "Vertex.hpp"
#include "VBO.hpp"

#include <vector>
#include <memory>

using uint = unsigned;

class TriangularMesh : public Geometry {
	
public:
	TriangularMesh();
	~TriangularMesh();
	void draw() override;
	void setVertices(std::vector<Vertex> newVertices);

protected:
	void initBuffers();

	std::vector<Vertex> vertices;
	std::unique_ptr<VBO> vbo;

};

