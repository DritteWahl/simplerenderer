#include "Material.hpp"

Material::Material() {
	diffuseColor = glm::vec3(0.0f, 0.0f, 0.0f);
	specularColor = glm::vec3(1.0f, 1.0f, 1.0f);
	shininess = 0.0f;
}

Material::~Material() {
	
}

void Material::bind(GLint program) {
	GLint matDiffuseColorLoc = glGetUniformLocation(program, "matDiffuseColor");
	GLint matSpecularColorLoc = glGetUniformLocation(program, "matSpecularColor");
	GLint shininessLoc = glGetUniformLocation(program, "shininess");
	GLint hasMaterialLoc = glGetUniformLocation(program, "hasMaterial");

	glUniform3f(matDiffuseColorLoc, diffuseColor.x, diffuseColor.y, diffuseColor.z);
	glUniform3f(matSpecularColorLoc, specularColor.x, specularColor.y, specularColor.z);
	glUniform1f(shininessLoc, shininess);
	glUniform1ui(hasMaterialLoc, true);
}

void Material::setDiffuseColor(float red, float green, float blue) {
	diffuseColor = glm::vec3(red, green, blue);
}

glm::vec3 Material::getDiffuseColor() {
	return diffuseColor;
}

void Material::setSpecularColor(float red, float green, float blue) {
	specularColor = glm::vec3(red, green, blue);
}

glm::vec3 Material::getSpecularColor() {
	return specularColor;
}

void Material::setShininess(float newShininess) {
	shininess = newShininess;
}

float Material::getShininess() {
	return shininess;
}


