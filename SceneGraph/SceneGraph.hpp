#pragma once

#include "Node.hpp"
#include <memory>
#include "Window/RenderContext.hpp"
#include "Tools/MathUtil.hpp"

class SceneGraph {
	
public:
	SceneGraph();
	SceneGraph(Node* root);
	~SceneGraph();

	void drawScene(RenderContext* context);
	void updateScene();
	uint getNewId();
	Node* getNode(uint searchId);
	Node* getRoot();
	void setRoot(Node* newRoot);
	void reloadShaders();
	void clear();

private:
	std::unique_ptr<Node> root;
	uint currentId = 0;

};
