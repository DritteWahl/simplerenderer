#include "Plane.hpp"

#include "SceneGraph/TriangularMesh.hpp"

Plane::Plane() : GeometricObject() {

	auto mesh = new TriangularMesh();
	std::vector<Vertex> vertices;

	const auto pos1 = glm::vec3(-0.5, -0.5, 0.0);
	const auto pos2 = glm::vec3(0.5, -0.5, 0.0);
	const auto pos3 = glm::vec3(-0.5, 0.5, 0.0);
	const auto pos4 = glm::vec3(0.5, 0.5, 0.0);
	const auto color = glm::vec3(0.5, 0.5, 0.5);
	const auto normal = glm::vec3(0.0, 0.0, 1.0);

	// front
	auto vertex1 = Vertex(pos1, normal, color, glm::vec2(0.0, 0.0));
	auto vertex2 = Vertex(pos2, normal, color, glm::vec2(1.0, 0.0));
	auto vertex3 = Vertex(pos3, normal, color, glm::vec2(0.0, 1.0));
	auto vertex4 = Vertex(pos4, normal, color, glm::vec2(1.0, 1.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	mesh->setVertices(vertices);

	setGeometry(mesh);

	isIlluminated = true;
}

Plane::~Plane() {}
