#include "Cube.hpp"

#include "SceneGraph/TriangularMesh.hpp"

Cube::Cube() : GeometricObject() {

	auto mesh = new TriangularMesh();
	std::vector<Vertex> vertices;

	const auto pos1 = glm::vec3(-0.5, -0.5, 0.5);
	const auto pos2 = glm::vec3(0.5, -0.5, 0.5);
	const auto pos3 = glm::vec3(-0.5, 0.5, 0.5);
	const auto pos4 = glm::vec3(0.5, 0.5, 0.5);
	const auto pos5 = glm::vec3(-0.5, -0.5, -0.5);
	const auto pos6 = glm::vec3(0.5, -0.5, -0.5);
	const auto pos7 = glm::vec3(-0.5, 0.5, -0.5);
	const auto pos8 = glm::vec3(0.5, 0.5, -0.5);
	const auto color = glm::vec3(0.5, 0.5, 0.5);
	const auto normalFront = glm::vec3(0.0, 0.0, 1.0);
	const auto normalBack = glm::vec3(0.0, 0.0, -1.0);
	const auto normalLeft = glm::vec3(-1.0, 0.0, 0.0);
	const auto normalRight = glm::vec3(1.0, 0.0, 0.0);
	const auto normalTop = glm::vec3(0.0, 1.0, 0.0);
	const auto normalBottom = glm::vec3(0.0, -1.0, 0.0);

	// front
	auto vertex1 = Vertex(pos1, normalFront, color, glm::vec2(0.5, 2.0 / 3.0));
	auto vertex2 = Vertex(pos2, normalFront, color, glm::vec2(0.75, 2.0 / 3.0));
	auto vertex3 = Vertex(pos3, normalFront, color, glm::vec2(0.5, 1.0 / 3.0));
	auto vertex4 = Vertex(pos4, normalFront, color, glm::vec2(0.75, 1.0 / 3.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	// top
	vertex1 = Vertex(pos3, normalTop, color, glm::vec2(0.5, 1.0));
	vertex2 = Vertex(pos4, normalTop, color, glm::vec2(0.75, 1.0));
	vertex3 = Vertex(pos7, normalTop, color, glm::vec2(0.5, 2.0 / 3.0));
	vertex4 = Vertex(pos8, normalTop, color, glm::vec2(0.75, 2.0 / 3.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	// back
	vertex1 = Vertex(pos6, normalBack, color, glm::vec2(0.0, 2.0 / 3.0));
	vertex2 = Vertex(pos5, normalBack, color, glm::vec2(0.25, 2.0 / 3.0));
	vertex3 = Vertex(pos8, normalBack, color, glm::vec2(0.0, 1.0 / 3.0));
	vertex4 = Vertex(pos7, normalBack, color, glm::vec2(0.25, 1.0 / 3.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	// bottom
	vertex1 = Vertex(pos5, normalBottom, color, glm::vec2(0.5, 1.0 / 3.0));
	vertex2 = Vertex(pos6, normalBottom, color, glm::vec2(0.75, 1.0 / 3.0));
	vertex3 = Vertex(pos1, normalBottom, color, glm::vec2(0.5, 0.0));
	vertex4 = Vertex(pos2, normalBottom, color, glm::vec2(0.75, 0.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	// left
	vertex1 = Vertex(pos5, normalLeft, color, glm::vec2(0.25, 2.0 / 3.0));
	vertex2 = Vertex(pos1, normalLeft, color, glm::vec2(0.5, 2.0 / 3.0));
	vertex3 = Vertex(pos7, normalLeft, color, glm::vec2(0.25, 1.0 / 3.0));
	vertex4 = Vertex(pos3, normalLeft, color, glm::vec2(0.5, 1.0 / 3.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	// right
	vertex1 = Vertex(pos2, normalRight, color, glm::vec2(0.75, 2.0 / 3.0));
	vertex2 = Vertex(pos6, normalRight, color, glm::vec2(1.0, 2.0 / 3.0));
	vertex3 = Vertex(pos4, normalRight, color, glm::vec2(0.75, 1.0 / 3.0));
	vertex4 = Vertex(pos8, normalRight, color, glm::vec2(1.0, 1.0 / 3.0));
	vertices.push_back(vertex1);
	vertices.push_back(vertex2);
	vertices.push_back(vertex3);
	vertices.push_back(vertex4);
	vertices.push_back(vertex3);
	vertices.push_back(vertex2);

	mesh->setVertices(vertices);

	setGeometry(mesh);

	isIlluminated = true;
}

Cube::~Cube() {}
