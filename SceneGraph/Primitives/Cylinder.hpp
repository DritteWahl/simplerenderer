#pragma once

#include "SceneGraph/GeometricObject.hpp"

class Cylinder : public GeometricObject {

public:
	Cylinder();
	Cylinder(int _resolution);
	~Cylinder();

private:
	int resolution;

};
