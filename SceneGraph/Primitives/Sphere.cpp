#include "Sphere.hpp"

#include "SceneGraph/TriangularMesh.hpp"
#include "Tools/MathUtil.hpp"

Sphere::Sphere() : Sphere(0) {

}

Sphere::Sphere(int _resolution) : GeometricObject() {

	resolution = _resolution;

	auto mesh = new TriangularMesh();
	std::vector<std::vector<Vertex>> vertices;
	std::vector<Vertex> triangleVertices;

	// create vertices
	for (size_t i = 0; i < resolution + 3; ++i) {
		float phi = (PI / 2.0) + ((i * PI) / (resolution + 2.0));
		std::vector<Vertex> circle;
		for (size_t j = 0; j <= resolution + 4; ++j) {
			float theta = (2.0 * j * PI) / (resolution + 4.0);
			const auto pos = glm::vec3(0.5 * cos(phi) * cos(theta), 0.5 * cos(phi) * sin(theta), 0.5 * sin(phi));
			const auto normal = glm::normalize(pos);
			const auto color = glm::vec3(0.5, 0.5, 0.5);
			const auto uv = glm::vec2(j / (resolution + 4.0), i / (resolution + 3.0));
			circle.emplace_back(pos, normal, color, uv);
		}
		vertices.push_back(circle);
	}

	// arrange vertices to triangles
	for (size_t i = 0; i < vertices.size() - 1; ++i) {
		for (size_t j = 0; j < vertices[i].size() - 1; ++j) {
			triangleVertices.push_back(vertices[i][j]);
			triangleVertices.push_back(vertices[i + 1][j]);
			triangleVertices.push_back(vertices[i + 1][j + 1]);

			triangleVertices.push_back(vertices[i][j]);
			triangleVertices.push_back(vertices[i + 1][j + 1]);
			triangleVertices.push_back(vertices[i][j + 1]);
		}
	}

	mesh->setVertices(triangleVertices);

	setGeometry(mesh);

	isIlluminated = true;
}

Sphere::~Sphere() {}
