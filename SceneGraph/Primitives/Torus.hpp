#pragma once

#include "SceneGraph/GeometricObject.hpp"

class Torus : public GeometricObject {

public:
	Torus();
	Torus(int _resolution);
	Torus(int _resolution, float _minorRadius);
	~Torus();

private:
	int resolution;
	float minorRadius;

};
