#pragma once

#include "SceneGraph/GeometricObject.hpp"

class Sphere : public GeometricObject {

public:
	Sphere();
	Sphere(int _resolution);
	~Sphere();

private:
	int resolution;

};
