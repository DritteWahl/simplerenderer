#include "Cylinder.hpp"

#include "SceneGraph/TriangularMesh.hpp"
#include "Tools/MathUtil.hpp"

Cylinder::Cylinder() : Cylinder(0) {

}

Cylinder::Cylinder(int _resolution) : GeometricObject() {

	resolution = _resolution;

	auto mesh = new TriangularMesh();
	std::vector<Vertex> vertices;

	auto pos = glm::vec3(0.0, -0.5, 0.0);
	auto normal = glm::vec3(0.0, -1.0, 0.0);
	auto color = glm::vec3(0.5, 0.5, 0.5);
	Vertex bottomCenter(pos, normal, color, glm::vec2(0.25, 0.25));

	pos = glm::vec3(0.0, 0.5, 0.0);
	normal = glm::vec3(0.0, 1.0, 0.0);
	color = glm::vec3(0.5, 0.5, 0.5);
	Vertex topCenter(pos, normal, color, glm::vec2(0.75, 0.25));

	std::vector<glm::vec3> bottomCirclePositions;
	std::vector<glm::vec3> topCirclePositions;
	std::vector<glm::vec2> bottomCircleUV;
	std::vector<glm::vec2> topCircleUV;
	std::vector<glm::vec2> barrelBottomUV;
	std::vector<glm::vec2> barrelTopUV;
	std::vector<glm::vec3> barrelNormals;

	for (size_t j = 0; j < resolution + 3; ++j) {
		float phi = (2 * j * PI) / (resolution + 3);
		bottomCirclePositions.push_back(glm::vec3(0.5 * sin(phi), -0.5, 0.5 * cos(phi)));
		bottomCircleUV.push_back(glm::vec2(0.25 + (0.25 * sin(phi)), 0.25 + (0.25 * cos(phi))));
		barrelBottomUV.push_back(glm::vec2(j / (resolution + 3.0), 1.0));
		topCirclePositions.push_back(glm::vec3(0.5 * sin(phi), 0.5, 0.5 * cos(phi)));
		topCircleUV.push_back(glm::vec2(0.75 + (0.25 * sin(phi)), 0.25 + (0.25 * cos(phi))));
		barrelTopUV.push_back(glm::vec2(j / (resolution + 3.0), 0.5));
		barrelNormals.push_back(glm::normalize(glm::vec3(0.5 * sin(phi), 0.0, 0.5 * cos(phi))));
	}

	// bottom circle
	normal = glm::vec3(0.0, -1.0, 0.0);
	for (size_t i = 0; i < bottomCirclePositions.size() - 1; ++i) {
		vertices.push_back(bottomCenter);
		vertices.emplace_back(bottomCirclePositions[i + 1], normal, color, bottomCircleUV[i + 1]);
		vertices.emplace_back(bottomCirclePositions[i], normal, color, bottomCircleUV[i]);
	}
	vertices.push_back(bottomCenter);
	vertices.emplace_back(bottomCirclePositions[0], normal, color, bottomCircleUV[0]);
	vertices.emplace_back(bottomCirclePositions.back(), normal, color, bottomCircleUV.back());

	// top circle
	normal = glm::vec3(0.0, 1.0, 0.0);
	for (size_t i = 0; i < topCirclePositions.size() - 1; ++i) {
		vertices.push_back(topCenter);
		vertices.emplace_back(topCirclePositions[i], normal, color, topCircleUV[i]);
		vertices.emplace_back(topCirclePositions[i + 1], normal, color, topCircleUV[i + 1]);
	}
	vertices.push_back(topCenter);
	vertices.emplace_back(topCirclePositions.back(), normal, color, topCircleUV.back());
	vertices.emplace_back(topCirclePositions[0], normal, color, topCircleUV[0]);

	// cylinder barrel
	for (size_t i = 0; i < topCirclePositions.size() - 1; ++i) {
		// TODO calculate uv
		vertices.emplace_back(topCirclePositions[i], barrelNormals[i], color, barrelTopUV[i]);
		vertices.emplace_back(bottomCirclePositions[i], barrelNormals[i], color, barrelBottomUV[i]);
		vertices.emplace_back(topCirclePositions[i + 1], barrelNormals[i + 1], color, barrelTopUV[i + 1]);

		vertices.emplace_back(topCirclePositions[i + 1], barrelNormals[i + 1], color, barrelTopUV[i + 1]);
		vertices.emplace_back(bottomCirclePositions[i], barrelNormals[i], color, barrelBottomUV[i]);
		vertices.emplace_back(bottomCirclePositions[i + 1], barrelNormals[i + 1], color, barrelBottomUV[i + 1]);
	}
	vertices.emplace_back(topCirclePositions.back(), barrelNormals.back(), color, barrelTopUV.back());
	vertices.emplace_back(bottomCirclePositions.back(), barrelNormals.back(), color, barrelBottomUV.back());
	vertices.emplace_back(topCirclePositions[0], barrelNormals[0], color, barrelTopUV[0]);
	vertices.emplace_back(topCirclePositions[0], barrelNormals[0], color, barrelTopUV[0]);
	vertices.emplace_back(bottomCirclePositions.back(), barrelNormals.back(), color, barrelBottomUV.back());
	vertices.emplace_back(bottomCirclePositions[0], barrelNormals[0], color, barrelBottomUV[0]);

	mesh->setVertices(vertices);

	setGeometry(mesh);

	isIlluminated = true;
}

Cylinder::~Cylinder() {}
