#pragma once

#include "SceneGraph/GeometricObject.hpp"

class Cone : public GeometricObject {

public:
	Cone();
	Cone(int _resolution);
	~Cone();

private:
	int resolution;

};
