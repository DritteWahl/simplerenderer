#pragma once

#include "SceneGraph/GeometricObject.hpp"

class Plane : public GeometricObject {
	
public:
	Plane();
	~Plane();
};
