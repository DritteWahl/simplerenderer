#include "Cone.hpp"

#include "SceneGraph/TriangularMesh.hpp"
#include "Tools/MathUtil.hpp"

Cone::Cone() : Cone(0) {

}

Cone::Cone(int _resolution) : GeometricObject() {

	resolution = _resolution;

	auto mesh = new TriangularMesh();
	std::vector<Vertex> vertices;

	auto pos = glm::vec3(0.0, -0.5, 0.0);
	auto normal = glm::vec3(0.0, -1.0, 0.0);
	auto color = glm::vec3(0.5, 0.5, 0.5);
	auto uv = glm::vec2(0.25, 0.5);
	Vertex bottomCenter(pos, normal, color, uv);

	pos = glm::vec3(0.0, 0.5, 0.0);
	normal = glm::vec3(0.0, 1.0, 0.0);
	color = glm::vec3(0.5, 0.5, 0.5);
	uv = glm::vec2(0.75, 0.5);
	Vertex topCenter(pos, normal, color, uv);

	std::vector<glm::vec3> bottomCirclePositions;
	std::vector<glm::vec2> bottomCircleUV;
	std::vector<glm::vec2> coneUV;

	for (size_t j = 0; j < resolution + 3; ++j) {
		float phi = (2 * j * PI) / (resolution + 3);
		bottomCirclePositions.push_back(glm::vec3(0.5 * sin(phi), -0.5, 0.5 * cos(phi)));
		bottomCircleUV.push_back(glm::vec2(0.25 + (0.25 * sin(phi)), 0.5 + (0.5 * cos(phi))));
		coneUV.push_back(glm::vec2(0.75 + (0.25 * sin(phi)), 0.5 + (0.5 * cos(phi))));
	}

	// bottom circle
	normal = glm::vec3(0.0, -1.0, 0.0);
	for (size_t i = 0; i < bottomCirclePositions.size() - 1; ++i) {
		vertices.push_back(bottomCenter);
		vertices.emplace_back(bottomCirclePositions[i + 1], normal, color, bottomCircleUV[i + 1]);
		vertices.emplace_back(bottomCirclePositions[i], normal, color, bottomCircleUV[i]);
	}
	vertices.push_back(bottomCenter);
	vertices.emplace_back(bottomCirclePositions[0], normal, color, bottomCircleUV[0]);
	vertices.emplace_back(bottomCirclePositions.back(), normal, color, bottomCircleUV.back());

	// cone area
	for (size_t i = 0; i < bottomCirclePositions.size() - 1; ++i) {
		// TODO calculate uv
		vertices.emplace_back(topCenter);
		vertices.emplace_back(bottomCirclePositions[i], glm::normalize(bottomCirclePositions[i]), color, coneUV[i]);
		vertices.emplace_back(bottomCirclePositions[i + 1], glm::normalize(bottomCirclePositions[i + 1]), color, coneUV[i + 1]);
	}
	vertices.emplace_back(topCenter);
	vertices.emplace_back(bottomCirclePositions.back(), glm::normalize(bottomCirclePositions.back()), color, coneUV.back());
	vertices.emplace_back(bottomCirclePositions[0], glm::normalize(bottomCirclePositions[0]), color, coneUV[0]);

	mesh->setVertices(vertices);

	setGeometry(mesh);

	isIlluminated = true;
}

Cone::~Cone() {}
