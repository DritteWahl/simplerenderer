#include "Transform.hpp"

#include <iostream>

Transform::Transform() {
	translation = glm::vec3(0.0);
	rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
	scaling = glm::vec3(1.0f);
	local = glm::mat4(1.0f);
	global = glm::mat4(1.0f);
}

Transform::Transform(glm::mat4 mat) {
	translation = glm::vec3(mat[3][0], mat[3][1], mat[3][2]);

	float scaleX = glm::length(glm::vec3(mat[0]));
	float scaleY = glm::length(glm::vec3(mat[1]));
	float scaleZ = glm::length(glm::vec3(mat[2]));
	scaling = glm::vec3(scaleX, scaleY, scaleZ);

	glm::mat3 rotMat = glm::mat3 {
		mat[0][0] / scaleX, mat[0][1] / scaleX, mat[0][2] / scaleX,
		mat[1][0] / scaleY, mat[1][1] / scaleY, mat[1][2] / scaleY,
		mat[2][0] / scaleZ, mat[2][1] / scaleZ, mat[2][2] / scaleZ
	};
	rotation = glm::quat_cast(rotMat);

	updateLocal();
}

void Transform::translate(float x, float y, float z) {
	glm::vec3 transVec = glm::vec3(x, y, z);
	translation = translation + transVec;
	updateLocal();
}

void Transform::translateLocal(float x, float y, float z) {
	glm::vec4 transVec = glm::vec4(x, y, z, 1);
	transVec = transVec * rotation;
	translation = translation + glm::vec3(transVec);
	updateLocal();
}

void Transform::rotate(float xAngle, float yAngle, float zAngle) {
	rotateAroundAxis(xAngle, glm::vec3(1, 0, 0));
	rotateAroundAxis(yAngle, glm::vec3(0, 1, 0));
	rotateAroundAxis(zAngle, glm::vec3(0, 0, 1));
}

void Transform::rotateLocal(float xAngle, float yAngle, float zAngle) {
	glm::vec3 xAxis = rotation * glm::vec3(1, 0, 0);
	glm::vec3 yAxis = rotation * glm::vec3(0, 1, 0);
	glm::vec3 zAxis = rotation * glm::vec3(0, 0, 1);

	rotateAroundAxis(xAngle, xAxis);
	rotateAroundAxis(yAngle, yAxis);
	rotateAroundAxis(zAngle, zAxis);
}

void Transform::setEulerAngles(float x, float y, float z) {
	rotation = glm::quat(glm::vec3(x, y, z));
	updateLocal();
}

void Transform::rotateAroundAxis(float angle, glm::vec3 dir) {
	dir = dir * rotation;
	rotation = glm::rotate(rotation, angle, dir);
	updateLocal();
}

void Transform::rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center) {
	glm::quat rot = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
	dir = dir * rotation;
	rot = glm::rotate(rot, angle, dir);

	translate(-center.x, -center.y, -center.z);
	rotation = rotation * rot;
	//translation = translation * rot;
	translate(center.x, center.y, center.z);
}

void Transform::scale(float factor) {
	scale(factor, factor, factor);
}

void Transform::scale(float x, float y, float z) {
	glm::vec3 scaleVec = glm::vec3(x, y, z);
	scaling = scaling * scaleVec;
	updateLocal();
	//local = glm::scale(local, scaleVec);
}

void Transform::updateLocal() {
	local = glm::mat4(1.0f);
	local = glm::translate(local, translation);
	local = glm::scale(local, scaling);
	local = local * glm::mat4(rotation);

	/*glm::mat4 transMatrix = glm::mat4(1.0);
	transMatrix[3] = glm::vec4(translation, 1.0f);
	glm::mat4 rotMatrix = mat4_cast(rotation);
	glm::mat4 scaleMatrix = glm::mat4(1.0);
	scaleMatrix[0][0] = scaling[0];
	scaleMatrix[1][1] = scaling[1];
	scaleMatrix[2][2] = scaling[2];
	local = transMatrix * rotMatrix * scaleMatrix;*/
}

void Transform::updateGlobal(glm::mat4 M) {
	global = M * local;
}