#include "ShaderProgram.hpp"

#include "../Tools/Helpers.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <string>
#include <iostream>


ShaderProgram::ShaderProgram() {
	
}

ShaderProgram::~ShaderProgram() {
	cleanup();
}

void ShaderProgram::create() {
	vShader = loadShader(vShaderFile, GL_VERTEX_SHADER);
	fShader = loadShader(fShaderFile, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, vShader);
	glAttachShader(program, fShader);

	glLinkProgram(program);

	helpers::checkGLError();

	vShaderFile = vShaderFile;
	fShaderFile = fShaderFile;

}

void ShaderProgram::bind() {
	glUseProgram(program);

	helpers::checkGLError();
}

void ShaderProgram::release() {
	glUseProgram(0);

	helpers::checkGLError();
}

void ShaderProgram::bindMVP(glm::mat4 M, glm::mat4 V, glm::mat4 P) {
	glm::mat4 MVP = P * V * M;

	glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(V * M)));
	GLint normalMatrixLoc = glGetUniformLocation(program, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, false, glm::value_ptr(normalMatrix));

	if(hasSeperatedMVP) {
		GLint MLoc = glGetUniformLocation(program, "M");
		GLint VLoc = glGetUniformLocation(program, "V");
		GLint PLoc = glGetUniformLocation(program, "P");
		if(MLoc == -1 || VLoc == -1 || PLoc == -1) {
			// TODO better exception handling
			std :: cout << "WARNING: MVP-uniforms could not be set in shader program " << std::to_string(program) << "!" << std::endl;
			//throw std::runtime_error("ERROR: MVP-uniforms could not be set in shader program " + std::to_string(program) + "!");
		}

		glUniformMatrix4fv(MLoc, 1, false, glm::value_ptr(M));
		glUniformMatrix4fv(VLoc, 1, false, glm::value_ptr(V));
		glUniformMatrix4fv(PLoc, 1, false, glm::value_ptr(P));
	}

	else {
		GLint MVPLoc = glGetUniformLocation(program, "MVP");
		if (MVPLoc == -1) {
			// TODO better exception handling
			std::cout << "WARNING: MVP-uniforms could not be set in shader program " << std::to_string(program) << "!" << std::endl;
			//throw std::runtime_error("ERROR: MVP-uniforms could not be set in shader program " + std::to_string(program) + "!");
		}

		glUniformMatrix4fv(MVPLoc, 1, false, glm::value_ptr(MVP));
	}

	helpers::checkGLError();
}

void ShaderProgram::setSeperatedMVP(bool seperatedMVP) {
	hasSeperatedMVP = seperatedMVP;
}

void ShaderProgram::setShaders(std::string vShaderPath = "", std::string fShaderPath = "") {
	vShaderFile = vShaderPath;
	fShaderFile = fShaderPath;
}

GLint ShaderProgram::getProgram() {
	return program;
}

void ShaderProgram::setIntUniform(std::string name, int value) {
	GLint uniformLoc = glGetUniformLocation(program, name.c_str());
	glUniform1i(uniformLoc, value);
	helpers::checkGLError();
}

GLuint ShaderProgram::loadShader(std::string shaderPath, GLenum shaderType) {
	GLuint shader = glCreateShader(shaderType);

	std::string shaderCode = helpers::GetFileContent(shaderPath);
	const GLchar* shaderCodeAsCStr[] = {shaderCode.c_str()};
	GLint codeLength[] = {shaderCode.size()};
	glShaderSource(shader, 1, shaderCodeAsCStr, codeLength);

	glCompileShader(shader);
	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if(isCompiled == GL_FALSE) {
		// TODO better exception handling & logging
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> errorLog(maxLength+1);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

		glDeleteShader(shader);

		//throw std::runtime_error("Shader compilation of shader '" + shaderPath + "' failed: " + errorLog.data());
		std::cout << "Shader compilation of shader '" << shaderPath << "' failed: " << std::endl << errorLog.data() << std::endl;
		return shaderType == GL_VERTEX_SHADER ? vShader : fShader;
	}

	helpers::checkGLError();

	return shader;
}

void ShaderProgram::cleanup() {
	release();

	glDetachShader(program, vShader);
	glDetachShader(program, fShader);

	glDeleteProgram(program);

	glDeleteShader(vShader);
	glDeleteShader(fShader);

	helpers::checkGLError();
}

