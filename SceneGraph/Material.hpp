#pragma once

#include <glm/vec3.hpp>
#include <gl/glew.h>

class Material {
	
public:
	Material();
	~Material();

	void bind(GLint program);
	void setDiffuseColor(float red, float green, float blue);
	glm::vec3 getDiffuseColor();
	void setSpecularColor(float red, float green, float blue);
	glm::vec3 getSpecularColor();
	void setShininess(float newShininess);
	float getShininess();

private:
	glm::vec3 diffuseColor;
	glm::vec3 specularColor;
	float shininess;

};
