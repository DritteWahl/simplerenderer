#pragma once

#include <vector>
#include "SceneGraph/DirectionalLight.hpp"
#include "SceneGraph/PointLight.hpp"

class LightManager {
	
public:
	LightManager();
	~LightManager();

	void addPointLight(PointLight* light);
	void addDirectionalLight(DirectionalLight* light);
	std::vector<PointLight*> getPointLights();
	std::vector<DirectionalLight*> getDirectionalLights();

private:
	std::vector<PointLight*> pointLights;
	std::vector<DirectionalLight*> directionalLights;

};
