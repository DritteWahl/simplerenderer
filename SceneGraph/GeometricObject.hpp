#pragma once

#include "Geometry.hpp"
#include "ShaderProgram.hpp"
#include "Material.hpp"
#include "Texture.hpp"

class GeometricObject : public SceneObject {
	
public:
	GeometricObject();
	~GeometricObject();

	void draw(RenderContext* context) override;
	void setGeometry(Geometry* newGeometry);
	void setShaderProgram(ShaderProgram* newShaderProgram);
	void setMaterial(Material* newMaterial);
	void addTexture(Texture* newTexture);
	void setIsIlluminated(bool newIsIlluminated);
	void reloadShaders() override;
	

protected:
	void bindLights(RenderContext* context, glm::mat4 V, GLint program);
	void bindTextures();
	void releaseTextures();

	Geometry* geometry = nullptr;
	ShaderProgram* shaderProgram = nullptr;
	Material* material = nullptr;
	std::vector<Texture*> textures;

	bool isIlluminated;
	bool hasMaterial;
};
