#include "VBO.hpp"

#include <vector>
#include "../Tools/Helpers.hpp"
#include <string>

VBO::VBO() {
	vbo = 0;
	vao = 0;

	initBuffers();
}

VBO::~VBO() {
	cleanup();
}

void VBO::feed(std::vector<Vertex> vertices) {
	const size_t sizeInBytes = vertices.size() * sizeof(Vertex);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);	
	glBufferData(GL_ARRAY_BUFFER, sizeInBytes, vertices.data(), GL_STATIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	helpers::checkGLError();

}

void VBO::bind() {
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	for (GLuint i = 0; i < vertexAttributeCount; i++) {
		glEnableVertexAttribArray(i);
	}

	helpers::checkGLError();
}

void VBO::release() {
	for (GLuint i = 0; i < vertexAttributeCount; i++) {
		glDisableVertexAttribArray(i);
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	helpers::checkGLError();
}

void VBO::initBuffers() {
	// TODO implement index buffer
	vertexAttributeCount = 4;

	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBindVertexArray(vao);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(3 * sizeof(GLfloat)));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(6 * sizeof(GLfloat)));
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(9 * sizeof(GLfloat)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	helpers::checkGLError();
}

void VBO::cleanup() {
	release();
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &vao);

	helpers::checkGLError();
}
