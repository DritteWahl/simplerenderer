#pragma once

#include <glm/vec3.hpp>
#include "SceneObject.hpp"

class LightSource : public SceneObject {
	
public:
	LightSource();
	LightSource(glm::vec3 color, float intensity);
	virtual ~LightSource();

	virtual void setColor(float red, float green, float blue);
	virtual glm::vec3 getColor();
	virtual void setIntensity(float newIntensity);
	virtual float getIntensity();

protected:
	glm::vec3 color;
	float intensity;
	

};
