#include "GeometricObject.hpp"
#include <iostream>

#include "Tools/Helpers.hpp"
#include "Tools/MathUtil.hpp"


GeometricObject::GeometricObject() : SceneObject() {
	isIlluminated = false;
	hasMaterial = false;
}

GeometricObject::~GeometricObject() {
	
}

void GeometricObject::draw(RenderContext* context) {	
	shaderProgram->bind();

	GLint program = shaderProgram->getProgram();

	glm::mat4 M = transform->global;
	glm::mat4 V = context->view;
	glm::mat4 P = context->projection;
	shaderProgram->bindMVP(M, V, P);

	if (isIlluminated) {
		bindLights(context, V, program);
	}

	if (hasMaterial) {
		material->bind(program);
	}
	else {
		shaderProgram->setIntUniform("hasMaterial", false);
	}

	bindTextures();

	geometry->draw();

	releaseTextures();
	shaderProgram->release();
}

void GeometricObject::setGeometry(Geometry* newRenderModel) {
	geometry = newRenderModel;
}

void GeometricObject::setShaderProgram(ShaderProgram* newShaderProgram) {
	shaderProgram = newShaderProgram;
}

void GeometricObject::setMaterial(Material* newMaterial) {
	material = newMaterial;
	hasMaterial = true;
}

void GeometricObject::addTexture(Texture* newTexture) {
	textures.push_back(newTexture);
}

void GeometricObject::setIsIlluminated(bool newIsIlluminated) {
	isIlluminated = newIsIlluminated;
}

void GeometricObject::reloadShaders() {
	shaderProgram->create();
}

void GeometricObject::bindLights(RenderContext* context, glm::mat4 V, GLint program) {
	std::vector<glm::vec3> transformedPointLightPos;
	for (auto lightPos : context->pointLightPositions) {
		transformedPointLightPos.emplace_back(glm::vec3(V * glm::vec4(lightPos, 1.0)));
	}

	std::vector<glm::vec3> transformedDirLightDirection;
	for (auto lightDir : context->dirLightDirections) {
		auto dir = glm::normalize(glm::vec3(V * glm::vec4(lightDir, 0.0)));
		transformedDirLightDirection.emplace_back(dir);
	}

	GLint pointLightsCount = (int) transformedPointLightPos.size();
	GLsizei dirLightsCount = (int) transformedDirLightDirection.size();

	GLint pointLightsCountLoc = glGetUniformLocation(program, "pointLightsCount");
	GLint pointLightColorsLoc = glGetUniformLocation(program, "pointLightColors");
	GLint pointLightIntensitiesLoc = glGetUniformLocation(program, "pointLightIntensities");
	GLint pointLightAttenuationsLoc = glGetUniformLocation(program, "pointLightAttenuations");
	GLint pointLightPositionsLoc = glGetUniformLocation(program, "pointLightPositions");
	GLint dirLightsCountLoc = glGetUniformLocation(program, "dirLightsCount");
	GLint dirLightColorsLoc = glGetUniformLocation(program, "dirLightColors");
	GLint dirLightIntensitiesLoc = glGetUniformLocation(program, "dirLightIntensities");
	GLint dirLightDirectionsLoc = glGetUniformLocation(program, "dirLightDirections");

	glUniform1i(pointLightsCountLoc, pointLightsCount);
	glUniform3fv(pointLightColorsLoc, pointLightsCount, &(context->pointLightColors.data()[0][0]));
	glUniform1fv(pointLightIntensitiesLoc, pointLightsCount, &(context->pointLightIntensities.data()[0]));
	glUniform3fv(pointLightAttenuationsLoc, pointLightsCount, &(context->pointLightAttenuations.data()[0][0]));
	glUniform3fv(pointLightPositionsLoc, pointLightsCount, &(transformedPointLightPos.data()[0][0]));
	glUniform1i(dirLightsCountLoc, dirLightsCount);
	glUniform3fv(dirLightColorsLoc, dirLightsCount, &(context->dirLightColors.data()[0][0]));
	glUniform1fv(dirLightIntensitiesLoc, dirLightsCount, &(context->dirLightIntensities.data()[0]));
	glUniform3fv(dirLightDirectionsLoc, dirLightsCount, &(transformedDirLightDirection.data()[0][0]));

	helpers::checkGLError();
}

void GeometricObject::bindTextures() {
	for (int i = 0; i < textures.size(); ++i) {
		textures[i]->bind(shaderProgram, i + 1);
	}
}

void GeometricObject::releaseTextures() {
	for (int i = 0; i < textures.size(); ++i) {
		textures[i]->release(shaderProgram, i + 1);
	}
}
