#include "PointLight.hpp"

#include "Node.hpp"

PointLight::PointLight() : LightSource() {
	attenuation = glm::vec3(0.0f, 0.0f, 0.0f);
}

PointLight::~PointLight() {
	
}

void PointLight::setAttenuation(float constant, float linear, float quadratic) {
	attenuation = glm::vec3(constant, linear, quadratic);
}

glm::vec3 PointLight::getAttenuation() {
	return attenuation;
}
