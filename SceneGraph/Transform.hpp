#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

struct Transform {

	glm::vec3 translation;
	glm::quat rotation;
	glm::vec3 scaling;
	glm::mat4 local;
	glm::mat4 global;

	Transform();
	Transform(glm::mat4 mat);

	void translate(float x, float y, float z);
	void translateLocal(float x, float y, float z);

	void rotate(float xAngle, float yAngle, float zAngle);
	void rotateLocal(float xAngle, float yAngle, float zAngle);
	void rotateAroundAxis(float angle, glm::vec3 dir);
	void rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center);
	void setEulerAngles(float x, float y, float z);

	void scale(float factor);
	void scale(float x, float y, float z);

	void updateLocal();
	void updateGlobal(glm::mat4 M);

};
