#pragma once

#include "LightSource.hpp"

class DirectionalLight : public LightSource {
	
public:
	DirectionalLight();
	~DirectionalLight();

	glm::vec3 getDirection();
};