#pragma once

#include <glm/glm.hpp>
#include "../Window/RenderContext.hpp"

class Geometry {
	
public:
	Geometry() {}
	virtual ~Geometry() {}
	virtual void draw() = 0;



};
