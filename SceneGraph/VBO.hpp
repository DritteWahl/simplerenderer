#pragma once

#include <vector>
#include "Vertex.hpp"
#include <gl/glew.h>

class VBO {
	
public:
	VBO();
	~VBO();
	void feed(std::vector<Vertex> vertices);
	void bind();
	void release();

private:
	void initBuffers();
	void cleanup();

	GLuint vbo;
	GLuint vao;

	size_t vertexAttributeCount; 
};
