#include "Node.hpp"

#include <string>

Node::Node(uint id) : id(id) {
	sceneObject = std::unique_ptr<SceneObject>(new SceneObject());
	sceneObject->setNode(this);
}

Node::Node(uint id, Node* parent) : Node(id) {
	setParent(parent);
}

uint Node::getId() {
	return id;
}

void Node::setSceneObject(SceneObject* newSceneObject) {
	sceneObject = std::unique_ptr<SceneObject>(newSceneObject);
	sceneObject->setNode(this);
}

void Node::setParent(Node* newParent) {
	parent = newParent;
}

Node* Node::getChild(uint searchId) {
	if (id == searchId) {
		return this;
	}

	Node* result = nullptr;
	for (auto& child : children) {
		result = child->getChild(searchId);
		if (result != nullptr) {
			return result;
		}
	}
	return result;
}

void Node::changeParent(Node* newParent) {
	// TODO not implemented yet
	throw std::runtime_error("Node::changeParent not implemented yet!");
}

void Node::remove() {
	if (parent != nullptr) {
		parent->removeChild(id);
	}
}

void Node::addChild(Node* newNode) {
	children.emplace_back(std::unique_ptr<Node>(newNode));
	newNode->setParent(this);
}

void Node::removeChild(uint childId) {
	for (auto it = children.begin(); it != children.end(); ++it) {
		if ((*it)->getId() == childId) {
			children.erase(it);
			break;
		}
	}
}

void Node::update(glm::mat4 M) {
	glm::mat4 newM = sceneObject->update(M);

	for (auto& child : children) {
		child->update(newM);
	}
}

void Node::draw(RenderContext* context) {
	sceneObject->draw(context);

	for (auto& child : children) {
		child->draw(context);
	}
}

void Node::reloadShaders() {
	sceneObject->reloadShaders();

	for (auto& child : children) {
		child->reloadShaders();
	}
}

std::string Node::toString() {
	std::string typeName = typeid(*(this->sceneObject)).name();
	std::string result = "{" + std::to_string(id) + ": " + typeName;
	result += children.empty() ? "" : " -> ";

	for (size_t i = 0; i < children.size(); ++i) {
		result += children[i]->toString();
		if (i < children.size() - 1) {
			result += ", ";
		}
	}

	result += "}";

	return result;
}
