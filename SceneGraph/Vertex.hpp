#pragma once

#include <glm/glm.hpp>

struct Vertex {
	glm::vec3 pos = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 normal = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 color = glm::vec3(0.0, 0.0, 0.0);
	glm::vec2 uv = glm::vec2(0.0, 0.0);

	Vertex() {}
	Vertex(glm::vec3 pos) : pos(pos) {}
	Vertex(glm::vec3 pos, glm::vec3 normal) : pos(pos), normal(normal) {}
	Vertex(glm::vec3 pos, glm::vec3 normal, glm::vec3 color) : pos(pos), normal(normal), color(color) {}
	Vertex(glm::vec3 pos, glm::vec3 normal, glm::vec2 uv) : pos(pos), normal(normal), uv(uv) {}
	Vertex(glm::vec3 pos, glm::vec3 normal, glm::vec3 color, glm::vec2 uv) : pos(pos), normal(normal), color(color), uv(uv) {}
};
