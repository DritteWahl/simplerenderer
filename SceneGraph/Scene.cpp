#include "Scene.hpp"

#include "FileImport/SceneImporter.hpp"

Scene::Scene(std::string name) : name(name) {
	sceneGraph = std::unique_ptr<SceneGraph>(new SceneGraph());
	cameraManager = std::unique_ptr<CameraManager>(new CameraManager());
	lightManager = std::unique_ptr<LightManager>(new LightManager());
}

Scene::~Scene() {
	
}

void Scene::updateScene() {
	sceneGraph->updateScene();
}

void Scene::drawScene() {
	RenderContext* renderContext = createRenderContext();

	sceneGraph->drawScene(renderContext);

	delete renderContext;
}

std::string Scene::getName() {
	return name;
}

void Scene::reloadShaders() {
	sceneGraph->reloadShaders();
}

SceneObject* Scene::loadFile(std::string filePath, ShaderProgram* shader, SceneObject* parent) {
	SceneImporter::setShaderProgram(shader);
	SceneObject* sceneObject = SceneImporter::importFile(filePath, this, parent);

	return sceneObject;
}

void Scene::loadFileAsNewScene(std::string filePath, ShaderProgram* shader) {
	sceneGraph->clear();
	loadFile(filePath, shader);
}

void Scene::addSceneObject(SceneObject* newObject) {
	auto node = new Node(sceneGraph->getNewId());
	node->setSceneObject(newObject);
	sceneGraph->getRoot()->addChild(node);
}

void Scene::addSceneObject(SceneObject* newObject, SceneObject* parent) {
	auto node = new Node(sceneGraph->getNewId());
	node->setSceneObject(newObject);
	parent->getNode()->addChild(node);
}

void Scene::addCamera(Camera* camera) {
	cameraManager->addCamera(camera);

	auto node = new Node(sceneGraph->getNewId());
	node->setSceneObject(camera);
	sceneGraph->getRoot()->addChild(node);
}

void Scene::addCamera(Camera* camera, SceneObject* parent) {
	cameraManager->addCamera(camera);

	auto node = new Node(sceneGraph->getNewId());
	node->setSceneObject(camera);
	parent->getNode()->addChild(node);
}

void Scene::addPointLight(PointLight* light) {
	lightManager->addPointLight(light);
	addSceneObject(light);
}

void Scene::addPointLight(PointLight* light, SceneObject* parent) {
	lightManager->addPointLight(light);
	addSceneObject(light, parent);
}

void Scene::addDirectionalLight(DirectionalLight* light) {
	lightManager->addDirectionalLight(light);
	addSceneObject(light);
}

void Scene::addDirectionalLight(DirectionalLight* light, SceneObject* parent) {
	lightManager->addDirectionalLight(light);
	addSceneObject(light, parent);
}

Camera* Scene::getActiveCamera() {
	return activeCamera;
}

void Scene::toggleCamera() {
	if (!activeCamera) {
		activeCamera = cameraManager->getFirstCamera();
	}
	else {
		activeCamera = cameraManager->getNextCamera(activeCamera->getNode()->getId());
	}
	
}

void Scene::toggleProjectionMode() {
	activeCamera->toggleProjectionMode();
}

RenderContext* Scene::createRenderContext() const {
	auto renderContext = new RenderContext();

	if (activeCamera != nullptr) {
		renderContext->view = activeCamera->getViewMatrix();
		renderContext->projection = activeCamera->getProjectionMatrix();
	}
	else {
		std::cout << "WARNING: No active camera selected in scene " << name.c_str() << ". Scene is rendered with standard camera." << std::endl;
	}

	setLightsInContext(renderContext);

	return renderContext;
}

void Scene::setLightsInContext(RenderContext* context) const {
	for (auto light : lightManager->getPointLights()) {
		glm::vec3 pos = glm::vec3(light->getGlobalTransform()[3]);
		context->pointLightPositions.push_back(pos);
		context->pointLightColors.push_back(light->getColor());
		context->pointLightIntensities.push_back(light->getIntensity());
		context->pointLightAttenuations.push_back(light->getAttenuation());
	}

	for (auto light : lightManager->getDirectionalLights()) {
		context->dirLightColors.push_back(light->getColor());
		context->dirLightIntensities.push_back(light->getIntensity());
		context->dirLightDirections.push_back(glm::vec3(light->getDirection()));
	}
}