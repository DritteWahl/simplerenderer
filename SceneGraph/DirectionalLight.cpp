#include "DirectionalLight.hpp"

#include "Node.hpp"

DirectionalLight::DirectionalLight() : LightSource() {

}

DirectionalLight::~DirectionalLight() {
	
}

glm::vec3 DirectionalLight::getDirection() {
	glm::quat rot = transform->rotation;
	return (glm::vec3(0.0f, 0.0f, 1.0f) * rot);
}
