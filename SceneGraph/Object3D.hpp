# pragma once
#include <glm/glm.hpp>
#include "../Window/RenderContext.hpp"

using uint = unsigned;

class Object3D : public SceneObject {
	
public:
	Object3D(Node* node) : SceneObject(node) {}
	virtual ~Object3D() {}
	//virtual void draw(RenderContext* context, glm::mat4 M) = 0;
	//virtual void reloadShaders() = 0;

};