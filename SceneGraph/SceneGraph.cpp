#include "SceneGraph.hpp"

#include "../Tools/Helpers.hpp"

SceneGraph::SceneGraph() {
	root = std::unique_ptr<Node>(new Node(getNewId()));
}

SceneGraph::SceneGraph(Node* root) : root(root) {

}

SceneGraph::~SceneGraph() {

}

void SceneGraph::drawScene(RenderContext* context) {
	root->draw(context);
}

void SceneGraph::updateScene() {
	//std::cout << root->toString() << std::endl;
	root->update(glm::mat4(1.0));
}

uint SceneGraph::getNewId() {
	uint id = currentId;
	currentId++;
	return id;
}

Node* SceneGraph::getNode(uint searchId) {
	return root->getChild(searchId);
}

Node* SceneGraph::getRoot() {
	return root.get();
}

void SceneGraph::setRoot(Node* newRoot) {
	root = std::unique_ptr<Node>(newRoot);
}

void SceneGraph::reloadShaders() {
	root->reloadShaders();
}

void SceneGraph::clear() {
	currentId = 0;
	root.release();
	root = std::unique_ptr<Node>(new Node(getNewId()));
}
