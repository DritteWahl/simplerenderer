#include "TriangularMesh.hpp"
#include <string>
#include "Tools/Helpers.hpp"

TriangularMesh::TriangularMesh() {
	vbo = std::unique_ptr<VBO>(new VBO());
}

TriangularMesh::~TriangularMesh() {

}

void TriangularMesh::draw() {
	vbo->bind();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	// Wireframe on
	glDrawArrays(GL_TRIANGLES, 0, (int)vertices.size());
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Wireframe off
	//glPointSize(3.0);
	//glDrawArrays(GL_POINTS, 0, (int)vertices.size());
	//glDrawArrays(GL_LINE_STRIP, 0, (int)vertices.size());

	helpers::checkGLError();

	vbo->release();
}

void TriangularMesh::initBuffers() {
	vbo->feed(vertices);
}

void TriangularMesh::setVertices(std::vector<Vertex> newVertices) {
	vertices = newVertices;
	initBuffers();
}
