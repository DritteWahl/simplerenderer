#pragma once

#include "SceneGraph/SceneGraph.hpp"
#include "SceneGraph/CameraManager.hpp"
#include "SceneGraph/LightManager.hpp"
#include "SceneGraph/GeometricObject.hpp"

class ShaderProgram;


class Scene {
	
public:
	Scene(std::string name);
	~Scene();

	void updateScene();
	void drawScene();
	std::string getName();
	void reloadShaders();

	SceneObject* loadFile(std::string filePath, ShaderProgram* shader, SceneObject* parent = nullptr);
	void loadFileAsNewScene(std::string filePath, ShaderProgram* shader);

	void addSceneObject(SceneObject*  newObject);
	void addSceneObject(SceneObject*  newObject, SceneObject* parent);
	void addCamera(Camera* camera);
	void addCamera(Camera* camera, SceneObject* parent);
	void addPointLight(PointLight* light);
	void addPointLight(PointLight* light, SceneObject* parent);
	void addDirectionalLight(DirectionalLight* light);
	void addDirectionalLight(DirectionalLight* light, SceneObject* parent);
	Camera* getActiveCamera();
	void toggleCamera();
	void toggleProjectionMode();
	RenderContext* createRenderContext() const;
	void setLightsInContext(RenderContext* context) const;

protected:
	std::string name;
	std::unique_ptr<SceneGraph> sceneGraph = nullptr;
	std::unique_ptr<CameraManager> cameraManager = nullptr;
	Camera* activeCamera = nullptr;
	std::unique_ptr<LightManager> lightManager = nullptr;

	ShaderProgram loadedSceneShader;

};
