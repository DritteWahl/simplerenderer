#pragma once

#include "SceneGraph/Camera.hpp"
#include <vector>
#include "Tools/MathUtil.hpp"

class CameraManager {

public:
	CameraManager();
	~CameraManager();

	void addCamera(Camera* camera);
	void removeCamera(uint id);
	Camera* getFirstCamera();
	Camera* getNextCamera(uint id);

private:
	std::vector<Camera*>::iterator getCameraIndex(uint id);

	std::vector<Camera*> cameraList;

};