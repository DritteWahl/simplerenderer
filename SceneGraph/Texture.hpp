#pragma once

#include <gl/glew.h>
#include <string>
#include "SceneGraph/ShaderProgram.hpp"

enum TextureType {
	DiffuseMap,
	NormalMap,
	SpecularMap,
	OpacityMap,
	DepthMap
};


class Texture {
	
public:
	Texture();
	Texture(TextureType _textureType);
	~Texture();

	void create();
	void bind(ShaderProgram* program, int texTarget);
	void release(ShaderProgram* program, int texTarget);
	void setTextureImage(std::string newPath);
	void setWrappingMode(GLenum newWrappingModeS, GLenum newWrappingModeT);
	void setFiltering(GLenum newMinFiltering, GLenum newMagFiltering);
	void setTexFormat(GLenum newTexFormat);
	void setTextureType(TextureType newTextureType);

protected:
	GLuint texId;
	std::string path = "";
	//std::string textureType;
	TextureType textureType;
	std::string textureTypeBool;

	GLenum wrappingModeS;
	GLenum wrappingModeT;
	GLenum minFiltering;
	GLenum magFiltering;
	GLenum texFormat;
	// TODO mipmaps
};
