#include "LightSource.hpp"

#include "Node.hpp"

LightSource::LightSource() : SceneObject() {
	color = glm::vec3(1.0f, 1.0f, 1.0f);
	intensity = 1.0f;
	
}

LightSource::LightSource(glm::vec3 color, float intensity) : SceneObject(), color(color), intensity(intensity) {
	
}

LightSource::~LightSource() {
	
}

void LightSource::setColor(float red, float green, float blue) {
	color = glm::vec3(red, green, blue);
}

glm::vec3 LightSource::getColor() {
	return color;
}

void LightSource::setIntensity(float newIntensity) {
	intensity = newIntensity;
}

float LightSource::getIntensity() {
	return intensity;
}
