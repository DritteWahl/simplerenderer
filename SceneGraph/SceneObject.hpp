#pragma once

#include <memory>
#include "Transform.hpp"

class Node;
struct RenderContext;

class SceneObject {
	
public:
	SceneObject();
	SceneObject(glm::mat4 transformMat);
	virtual ~SceneObject();

	virtual Node* getNode();
	virtual void setNode(Node* newNode);
	virtual glm::mat4 update(glm::mat4 M);
	virtual void draw(RenderContext* context);
	virtual void reloadShaders();

	virtual glm::mat4 getLocalTransform();
	virtual glm::mat4 getGlobalTransform();
	virtual void translate(float x, float y, float z);
	virtual void translateLocal(float x, float y, float z);
	virtual void rotate(float xAngle, float yAngle, float zAngle);
	virtual void rotateLocal(float xAngle, float yAngle, float zAngle);
	virtual void rotateAroundAxis(float angle, glm::vec3 dir);
	virtual void rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center);
	virtual void setEulerAngles(float roll, float pitch, float yaw);
	virtual void scale(float factor);
	virtual void scale(float x, float y, float z);

protected:
	Node* node;
	std::unique_ptr<Transform> transform;

};
