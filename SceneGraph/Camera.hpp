#pragma once

#include <memory>
#include "SceneObject.hpp"

class Camera : public SceneObject {
	
public:
	Camera();
	~Camera();

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();
	void setPerspectiveProjection(bool isPerspective);
	void toggleProjectionMode();
	void setZNear(float zNear);
	void setZFar(float zFar);
	void setFov(float fov);
	void setAspectRatio(float width, float height);

	void translate(float x, float y, float z) override;
	void translateLocal(float x, float y, float z) override;
	void setEulerAngles(float roll, float pitch, float yaw) override;
	void rotateAroundAxis(float angle, glm::vec3 dir) override;
	void rotateAroundPoint(float angle, glm::vec3 dir, glm::vec3 center) override;
	void scale(float factor) override;
	void scale(float x, float y, float z) override;

	void toggleDemoMode();
	void setDemoSpeed(float speed);
	glm::mat4 update(glm::mat4 M) override;

protected:
	glm::mat4 projection = glm::mat4(1.0);
	bool isPerspective = true;
	float zNear = 0.0f;
	float zFar = 0.0f;
	float fov = 0.0f;
	float width = 0.0f;
	float height = 0.0f;

	bool demoMode = false;
	float demoSpeed = 0.001f;

	void updateProjection();

};
