#pragma once

#include <glm/glm.hpp>
#include <ostream>
#include <iostream>

#define PI 3.14159265359f

using uint = unsigned;

inline std::ostream& operator << (std::ostream& os, glm::mat4 const& value) {
	os << "(";
	for(int i = 0; i < 4; i++) {
		os << "(";
		for (int j = 0; j < 4; j++) {	
			os << value[i][j];
			if (j < 3) {
				os << ",";
			}
		}
		os << ")";
		if(i < 3) {
			os << ",";
		}
	}
	os << ")";
	return os;
}

inline std::ostream& operator << (std::ostream& os, glm::mat3 const& value) {
	os << "(";
	for (int i = 0; i < 3; i++) {
		os << "(";
		for (int j = 0; j < 3; j++) {
			os << value[i][j];
			if (j < 3) {
				os << ",";
			}
		}
		os << ")";
		if (i < 2) {
			os << ",";
		}
	}
	os << ")";
	return os;
}

inline std::ostream& operator << (std::ostream& os, glm::vec4 const& value) {
	os << "(" << value[0] << "," << value[1] << "," << value[2] << "," << value[3] << ")";
	return os;
}

inline std::ostream& operator << (std::ostream& os, glm::vec3 const& value) {
	os << "(" << value[0] << "," << value[1] << "," << value[2] << ")";
	return os;
}

inline bool approxEquals(float f1, float f2, float epsilon = 0.0001) {
	return (std::abs(f1 - f2) < epsilon);
}

inline bool approxEquals(const glm::mat4& m1, const glm::mat4& m2, float epsilon = 0.001) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (!approxEquals(m1[i][j], m2[i][j], epsilon)) {
				return false;
			}
		}
	}
	return true;
}