#pragma once

#include <gl/glew.h>
#include <string>
#include <fstream>
#include <sstream>

namespace helpers {

	inline void checkGLError() {
		GLenum error = glGetError();
		if (error != GL_NO_ERROR) {
			throw std::runtime_error("GL ERROR: " + std::to_string(error));
		}
	}

	inline std::string GetFileContent(std::string path) {
		const char* pathAsCStr = path.c_str();

		std::ifstream t(pathAsCStr);

		if(t.fail()) {
			throw std::runtime_error("ERROR: can not open file " + path);
		}

		std::stringstream buffer;
		buffer << t.rdbuf();

		std::string fileContent = buffer.str();

		return fileContent;
	}

}