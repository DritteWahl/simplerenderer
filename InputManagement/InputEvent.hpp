#pragma once

enum class InputType {
	Pressed, 
	Released, 
	Repeat
};

struct InputEvent {

	bool operator==(const InputEvent& other) const;
	bool operator<(const InputEvent& other) const;

	int sdlKey = 0;
	InputType type = InputType::Pressed;

};
