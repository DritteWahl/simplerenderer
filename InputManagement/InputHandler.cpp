#include "InputHandler.hpp"
#include <iostream>
#include "InputEvent.hpp"

InputHandler::InputHandler(Viewport* viewport) : viewport(viewport) {
	inputMapper = std::unique_ptr<InputMapper>(new InputMapper(viewport));
}

void InputHandler::handleEvent(SDL_Event* event) {
	switch (event->type) {

		case SDL_MOUSEBUTTONDOWN: {
			int button = static_cast<uint>(event->button.button);
			//std::cout << button << std::endl;
			uint clickCount = static_cast<uint>(event->button.clicks);
			bool doubleClick = false;
			if (clickCount > 1) {
				doubleClick = true;
			}
			InputType type = (InputType::Pressed);
			inputMapper->executeKeyAction(InputEvent{button, type});
			break;
		}

		case SDL_MOUSEBUTTONUP:
		{
			int button = static_cast<uint>(event->button.button);
			uint clickCount = static_cast<uint>(event->button.clicks);
			bool doubleClick = false;
			if (clickCount > 1) {
				doubleClick = true;
			}
			InputType type = (InputType::Released);
			inputMapper->executeKeyAction(InputEvent{button, type});
			break;
		}

		case SDL_MOUSEWHEEL:
		{
			int y = event->wheel.y;
			inputMapper->executeMouseScrollAction(y);
			break;
		}

		case SDL_MOUSEMOTION:
		{
			int x = event->motion.x;
			int y = event->motion.y;
			int relX = event->motion.xrel;
			int relY = event->motion.yrel;
			inputMapper->executeMouseMoveAction(relX, relY, x, y);
			break;
		}

		case SDL_KEYDOWN: {
			int key = event->key.keysym.sym;
			//std::cout << key << std::endl;
			bool repeat = event->key.repeat;
			InputType type = (repeat ? InputType::Repeat : InputType::Pressed);
			inputMapper->executeKeyAction(InputEvent{key, type});
			break;
		}

		case SDL_KEYUP:	{
			int key = event->key.keysym.sym;
			bool repeat = event->key.repeat;
			InputType type = InputType::Released;
			inputMapper->executeKeyAction(InputEvent{key, type});
			break;
		}

		default: {
			break;
		}

	}

}
