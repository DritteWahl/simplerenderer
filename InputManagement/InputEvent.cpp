#include "InputEvent.hpp"

bool InputEvent::operator==(const InputEvent& other) const {
	return (this->sdlKey == other.sdlKey && this->type == other.type);
}

bool InputEvent::operator<(const InputEvent& other) const {
	if (this->sdlKey == other.sdlKey) {
		return int(this->type) < int(other.type);
	}
	return (this->sdlKey < other.sdlKey);
}