#pragma once

#include "Window/Viewport.hpp"
#include "InputMapper.hpp"

class InputHandler {
	
public:
	InputHandler(Viewport* viewport);
	void handleEvent(SDL_Event* event);

private:
	Viewport* viewport;
	std::unique_ptr<InputMapper> inputMapper;

};
