#pragma once

class Action {
	
public:
	Action() { }
	virtual ~Action() { }
	virtual void execute() = 0;
	virtual void setReleased(bool newIsReleased) = 0;

};