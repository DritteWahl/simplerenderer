#include "MoveCameraAction.hpp"

MoveCameraAction::MoveCameraAction(Viewport* viewport, InputMapper* inputMapper, float x, float y, float z) 
	: ViewportAction(viewport, inputMapper), x(x), y(y), z(z) {
	
}

void MoveCameraAction::execute() {
	viewport->getScene()->getActiveCamera()->translateLocal(x, y, z);
}
