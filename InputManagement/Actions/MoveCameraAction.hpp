#pragma once

#include "ViewportAction.hpp"

class MoveCameraAction : public ViewportAction {
	
public:
	MoveCameraAction(Viewport* viewport, InputMapper* inputMapper, float x, float y, float z);
	void execute();
	
private:
	float x;
	float y;
	float z;

};
