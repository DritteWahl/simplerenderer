#include "RotationModeAction.hpp"

RotationModeAction::RotationModeAction(Viewport* viewport, InputMapper* inputMapper, bool on) 
		: ViewportAction(viewport, inputMapper) {
	turnOn = on;
}

void RotationModeAction::execute() {
	inputMapper->setRotationMode(turnOn);
}