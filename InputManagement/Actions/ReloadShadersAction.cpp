#include "ReloadShadersAction.hpp"

ReloadShadersAction::ReloadShadersAction(Viewport* viewport, InputMapper* inputMapper) : ViewportAction(viewport, inputMapper) {
	
}

void ReloadShadersAction::execute() {
	viewport->getScene()->reloadShaders();
}
