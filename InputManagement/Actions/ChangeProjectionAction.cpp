#include "ChangeProjectionAction.hpp"

ChangeProjectionAction::ChangeProjectionAction(Viewport* viewport, InputMapper* inputMapper) : ViewportAction(viewport, inputMapper) {
	
}

void ChangeProjectionAction::execute() {
	viewport->getScene()->toggleProjectionMode();
}
