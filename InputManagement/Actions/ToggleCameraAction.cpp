#include "ToggleCameraAction.hpp"

ToggleCameraAction::ToggleCameraAction(Viewport* viewport, InputMapper* inputMapper) : ViewportAction(viewport, inputMapper) {
	
}

void ToggleCameraAction::execute() {
	viewport->getScene()->toggleCamera();
}
