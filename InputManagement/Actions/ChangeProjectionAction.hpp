#pragma once

#include "ViewportAction.hpp"

class ChangeProjectionAction : public ViewportAction {

public:
	ChangeProjectionAction(Viewport* viewport, InputMapper* inputMapper);
	void execute();

};
