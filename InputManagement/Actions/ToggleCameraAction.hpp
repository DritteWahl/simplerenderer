#pragma once

#include "ViewportAction.hpp"

class ToggleCameraAction : public ViewportAction {

public:	
	ToggleCameraAction(Viewport* viewport, InputMapper* inputMapper);
	void execute() override;

};
