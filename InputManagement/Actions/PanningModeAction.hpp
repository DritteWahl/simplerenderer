#pragma once

#include "ViewportAction.hpp"

class PanningModeAction : public ViewportAction {

public:	
	PanningModeAction(Viewport* viewport, InputMapper* inputMapper, bool on);
	void execute() override;

protected:
	bool turnOn;

};
