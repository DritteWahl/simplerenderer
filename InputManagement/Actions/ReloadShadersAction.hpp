#pragma once

#include "ViewportAction.hpp"

class ReloadShadersAction : public ViewportAction {

public:
	ReloadShadersAction(Viewport* viewport, InputMapper* inputMapper);
	void execute() override;

};
