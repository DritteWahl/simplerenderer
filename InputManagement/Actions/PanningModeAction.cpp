#include "PanningModeAction.hpp"

PanningModeAction::PanningModeAction(Viewport* viewport, InputMapper* inputMapper, bool on) : ViewportAction(viewport, inputMapper) {
	turnOn = on;
}

void PanningModeAction::execute() {
	inputMapper->setPanningMode(turnOn);
}
