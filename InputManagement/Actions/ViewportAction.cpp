#include "ViewportAction.hpp"

ViewportAction::ViewportAction(Viewport* viewport, InputMapper* inputMapper) : viewport(viewport), inputMapper(inputMapper) {
	
}

ViewportAction::~ViewportAction() {
	
}

void ViewportAction::setReleased(bool newIsReleased) {
	isReleased = newIsReleased;
}
