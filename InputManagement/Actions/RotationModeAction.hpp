#pragma once

#include "ViewportAction.hpp"

class RotationModeAction : public ViewportAction {

public:	
	RotationModeAction(Viewport* viewport, InputMapper* inputMapper, bool on);
	void execute() override;

protected:
	bool turnOn;

};
