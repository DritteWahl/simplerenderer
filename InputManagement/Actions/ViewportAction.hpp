#pragma once

#include "Action.hpp"
#include "Window/Viewport.hpp"
#include "InputManagement/InputMapper.hpp"

class ViewportAction : public Action {
	
public:
	ViewportAction(Viewport* viewport, InputMapper* inputMapper);
	~ViewportAction();

	void setReleased(bool newIsReleased) override;

protected:
	Viewport* viewport = nullptr;
	InputMapper* inputMapper = nullptr;
	bool isReleased = false;

};

