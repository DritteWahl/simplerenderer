#pragma once

#include "Window/Viewport.hpp"
#include "Actions/Action.hpp"
#include "InputEvent.hpp"
#include <map>

enum class CameraMode {
	Rotation,
	Panning,
	Default
};

class InputMapper {

public:
	InputMapper(Viewport* viewport);
	~InputMapper();

	void loadInputMap();
	void executeKeyAction(const InputEvent&);
	void executeMouseButtonAction(const InputEvent& event);
	void executeMouseMoveAction(int relX, int relY, int x, int y);
	void executeMouseScrollAction (int y);
	void setRotationMode(bool mode);
	void setPanningMode(bool mode);

	void insertEvent(const InputEvent& event, Action* action);

private:
	void rotateCamera(int relX, int relY, Camera* camera);
	void panCamera(int relX, int relY, Camera* camera);
	void zoom(int y, Camera* camera);

	std::map<InputEvent, std::unique_ptr<Action>> inputMap;
	Viewport* viewport;

	float cameraMovementSpeed = 0.075f;
	float cameraRotationSpeed = 0.005f;
	float cameraPanningSpeed = 0.01f;
	float zoomingSpeed = 0.25f;
	CameraMode cameraMode = CameraMode::Default;

};
