#include "InputMapper.hpp"

#include "Actions/MoveCameraAction.hpp"
#include "Actions/ReloadShadersAction.hpp"
#include "Actions/ChangeProjectionAction.hpp"
#include "Actions/ToggleCameraAction.hpp"
#include "Actions/RotationModeAction.hpp"
#include "Actions/PanningModeAction.hpp"
#include <iostream>
#include "Tools/MathUtil.hpp"

InputMapper::InputMapper(Viewport* viewport) : viewport(viewport) {
	loadInputMap();
}

InputMapper::~InputMapper() {
	
}

void InputMapper::loadInputMap() {
	// TODO data-driven

	insertEvent({SDLK_w, InputType::Pressed}, new MoveCameraAction(viewport, this, 0.0f, 0.0f, -cameraMovementSpeed));
	insertEvent({SDLK_w, InputType::Repeat}, new MoveCameraAction(viewport, this, 0.0f, 0.0f, -cameraMovementSpeed));
	insertEvent({SDLK_s, InputType::Pressed}, new MoveCameraAction(viewport, this, 0.0f, 0.0f, cameraMovementSpeed));
	insertEvent({SDLK_s, InputType::Repeat}, new MoveCameraAction(viewport, this, 0.0f, 0.0f, cameraMovementSpeed));
	insertEvent({SDLK_a, InputType::Pressed}, new MoveCameraAction(viewport, this, -cameraMovementSpeed, 0.0f, 0.0f));
	insertEvent({SDLK_a, InputType::Repeat}, new MoveCameraAction(viewport, this, -cameraMovementSpeed, 0.0f, 0.0f));
	insertEvent({SDLK_d, InputType::Pressed}, new MoveCameraAction(viewport, this, cameraMovementSpeed, 0.0f, 0.0f));
	insertEvent({SDLK_d, InputType::Repeat}, new MoveCameraAction(viewport, this, cameraMovementSpeed, 0.0f, 0.0f));
	insertEvent({SDLK_q, InputType::Pressed}, new MoveCameraAction(viewport, this, 0.0f, cameraMovementSpeed, 0.0f));
	insertEvent({SDLK_q, InputType::Repeat}, new MoveCameraAction(viewport, this, 0.0f, cameraMovementSpeed, 0.0f));
	insertEvent({SDLK_e, InputType::Pressed}, new MoveCameraAction(viewport, this, 0.0f, -cameraMovementSpeed, 0.0f));
	insertEvent({SDLK_e, InputType::Repeat}, new MoveCameraAction(viewport, this, 0.0f, -cameraMovementSpeed, 0.0f));
	insertEvent({SDL_BUTTON_RIGHT, InputType::Pressed}, new RotationModeAction(viewport, this, true));
	insertEvent({SDL_BUTTON_RIGHT, InputType::Released}, new RotationModeAction(viewport, this, false));
	insertEvent({SDL_BUTTON_LEFT, InputType::Pressed}, new PanningModeAction(viewport, this, true));
	insertEvent({SDL_BUTTON_LEFT, InputType::Released}, new PanningModeAction(viewport, this, false));
	insertEvent({SDLK_r, InputType::Pressed}, new ReloadShadersAction(viewport, this));
	insertEvent({SDLK_c, InputType::Pressed}, new ToggleCameraAction(viewport, this));
	insertEvent({SDLK_p, InputType::Pressed}, new ChangeProjectionAction(viewport, this));
	
}


void InputMapper::executeKeyAction(const InputEvent& event) {
	auto actionIt = inputMap.find(event);
	if(actionIt != inputMap.end()) {
		actionIt->second->execute();
	}
}

void InputMapper::executeMouseButtonAction(const InputEvent& event) {
	auto actionIt = inputMap.find(event);
	if (actionIt != inputMap.end()) {
		actionIt->second->execute();
	}
}

void InputMapper::executeMouseMoveAction(int relX, int relY, int x, int y) {
	if (cameraMode == CameraMode::Rotation) {
		rotateCamera(relX, relY, viewport->getScene()->getActiveCamera());
	}
	if (cameraMode == CameraMode::Panning) {
		panCamera(relX, relY, viewport->getScene()->getActiveCamera());
	}
}

void InputMapper::executeMouseScrollAction(int y) {
	zoom(y, viewport->getScene()->getActiveCamera());
}

void InputMapper::setRotationMode(bool mode) {
	if (cameraMode == CameraMode::Default && mode) {
		cameraMode = CameraMode::Rotation;
	}
	else if (cameraMode == CameraMode::Rotation && !mode) {
		cameraMode = CameraMode::Default;
	}
}

void InputMapper::setPanningMode(bool mode) {
	if (cameraMode == CameraMode::Default && mode) {
		cameraMode = CameraMode::Panning;
	}
	else if (cameraMode == CameraMode::Panning && !mode) {
		cameraMode = CameraMode::Default;
	}
}

void InputMapper::insertEvent(const InputEvent& event, Action* action) {
	inputMap.emplace(event, std::unique_ptr<Action>(action));
}

void InputMapper::rotateCamera(int relX, int relY, Camera* camera) {
	glm::vec3 dir = glm::vec3(static_cast<float>(relY), static_cast<float>(relX), 0.0f);
	float length = glm::length(dir);
	if (glm::length(dir) > 0.0f) {
		glm::vec3 normalizedDir = glm::normalize(dir);
		float angle = (PI / 2.0f) * length * cameraRotationSpeed;
		//camera->rotateLocal(angle, normalizedDir);
		camera->rotateAroundPoint(angle, normalizedDir, glm::vec3(0.0, 0.0, 0.0));
	}
}

void InputMapper::panCamera(int relX, int relY, Camera* camera) {
	float xPan = -(relX * cameraPanningSpeed);
	float yPan = relY * cameraPanningSpeed;
	camera->translate(xPan, yPan, 0.0f);
}

void InputMapper::zoom(int y, Camera* camera) {
	float zoom = zoomingSpeed * (-y);
	camera->translate(0.0f, 0.0f, zoom);
}

