#version 450

// UNIFORMS
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat3 normalMatrix;

uniform int pointLightsCount;
uniform vec3[10] pointLightColors;
uniform float[10] pointLightIntensities;
uniform vec3[10] pointLightPositions;
uniform vec3[10] pointLightAttenuations;

uniform int dirLightsCount;
uniform vec3[10] dirLightColors;
uniform float[10] dirLightIntensities;
uniform vec3[10] dirLightDirections;

uniform bool hasMaterial;
uniform vec3 matDiffuseColor;
uniform vec3 matSpecularColor;
uniform float shininess;

uniform bool hasDiffuseMap = false;
uniform sampler2D diffuseMap;
uniform bool hasNormalMap = false;
uniform sampler2D normalMap;
uniform bool hasSpecularMap = false;
uniform sampler2D specularMap;
uniform bool hasOpacityMap = false;
uniform sampler2D opacityMap;
uniform bool hasDepthMap = false;
uniform sampler2D depthMap;

// TODO make uniform
float heightScale = 0.1;


// INPUT
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;
layout(location = 3) in vec2 uv;
layout(location = 4) in vec3 worldPos;
layout(location = 5) in vec3 worldNormal;

// OUTPUT
out vec4 finalColor;

// STRUCTS
struct PointLight {
	vec3 color;
	vec3 pos;
	float intensity;

	float constant;
	float linear;
	float quadratic;
};

struct DirLight {
	vec3 color;
	vec3 dir;
	float intensity;
};

vec3 stuff;


// SHADER CODE

vec2 parallaxMapping (vec2 texCoords, mat3 cotangentFrame) {
	 float height = texture(depthMap, texCoords).r;
	 vec3 viewDir = cotangentFrame * normalize(pos);
	 viewDir.z = -viewDir.z;
	 //viewDir.y = -viewDir.y;
	 viewDir = normalize(viewDir);

	 vec2 p = (viewDir.xy / viewDir.z) * (height * heightScale);
	 texCoords = texCoords + p;
	 stuff = vec3(1.0, 1.0, 1.0);

	 //return viewDir.xy;
	 return texCoords;
}


vec2 parallaxOcclusionMapping (vec2 texCoords, mat3 cotangentFrame) {
	 //float height = texture(depthMap, texCoords).r;
	 vec3 viewDir = cotangentFrame * normalize(pos);
	 viewDir.z = -viewDir.z;
	 //viewDir.y = -viewDir.y;
	 viewDir = normalize(viewDir);

	 const float minLayers = 16.0;
	 const float maxLayers = 64.0;
	 float layersCount = mix(maxLayers, minLayers, max(dot(vec3(0.0, 0.0, 1.0), viewDir), 0.0));
	 float layerDepth = 1.0 / layersCount;
	 float currDepth = 0.0;

	 vec2 view = vec2(-viewDir.x, -viewDir.y);
	 //vec2 p = viewDir.xy * heightScale;
	 vec2 p = view * heightScale;
	 vec2 deltaTexCoords = p / layersCount;

	 vec2 currTexCoords = texCoords;
	 float currDepthMapValue = texture(depthMap, currTexCoords).r;
	 float prevDepthMapValue = currDepthMapValue;
	 vec2 prevTexCoords = currTexCoords;

	 while (currDepth < currDepthMapValue) {
		prevDepthMapValue = currDepthMapValue;
		prevTexCoords = currTexCoords;
		currTexCoords -= deltaTexCoords;
		currDepthMapValue = texture(depthMap, currTexCoords).r;
		currDepth += layerDepth;
	 }

	 float afterDepth = currDepthMapValue - currDepth;
	 float beforeDepth = prevDepthMapValue - currDepth + layerDepth;
	 float weight = afterDepth / (afterDepth - beforeDepth);
	 vec2 finalTexCoords = prevTexCoords * weight + currTexCoords * (1.0 - weight);

	 //return viewDir.xy;
	 return finalTexCoords;
}


vec3 determineBaseColor (vec2 texCoords) {
	if (hasDiffuseMap) {
		vec4 texColor = texture(diffuseMap, texCoords);
		return texColor.xyz;
	}

	else if (hasMaterial) {
		return matDiffuseColor;
	}

	else {
		return color;
	}
}


float determineOpacity(vec2 texCoords) {
	return (1.0 - texture(opacityMap, texCoords).x);
}


mat3 computeCotangentFrame(vec3 N, vec2 texCoords) {
	vec3 dp1 = dFdx(pos);
	vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(texCoords);
	vec2 duv2 = dFdy(texCoords);

    // solve the linear system
    vec3 dp2perp = cross(dp2, N);
    vec3 dp1perp = cross(dp1, N);

    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    // float D = 1.0f / (dot(dp1, dp1) * dot(dp2, dp2) - pow(dot(dp1, dp2), 2)); // ||dp1 x dp2||�
    float D = inversesqrt(max(dot(T, T), dot(B, B)));
    // attention: T or B normalized, orthogonal to N
    mat3 cotangentFrame = mat3(T * D, B * D, N);

	return cotangentFrame;
}


vec3 computeNormalFromTexture(vec3 N, vec2 texCoords, mat3 cotangentFrame) {
	vec3 newN = normalize(texture2D(normalMap, texCoords).rgb * 2.0 - 1.0);
	newN = cotangentFrame * newN;
	return newN;
}


vec3 calculatePointLight(PointLight light, vec3 N, vec2 texCoords) {
	vec3 diffuseColor = determineBaseColor(texCoords);
	vec3 specularColor = vec3(1.0, 1.0, 1.0);
	float ambientIntensity = 0.1f;
	float shininessFactor = 16.0;

	if (hasMaterial) {
		specularColor = matSpecularColor;
		shininessFactor = shininess;
	}

	vec3 lightDir = normalize(light.pos - pos);
	float lambertian = max(dot(lightDir, N), 0.0);
	float specular = 0.0;

	if (lambertian > 0.0) {
		vec3 reflectDir = reflect(-lightDir, N);
		vec3 viewDir = normalize(-pos);

		vec3 halfDir = normalize(lightDir + viewDir);
		float specAngle = max(dot(halfDir, N), 0.0);
		specular = pow(specAngle, shininessFactor);
		if (hasSpecularMap) specular = specular * (texture(specularMap, texCoords).x);
	}

	float distance = length(light.pos - pos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + 
						light.quadratic * (distance * distance));

	vec3 ambientColor = light.color * ambientIntensity * diffuseColor;
	diffuseColor = light.color * lambertian * diffuseColor;
	specularColor = light.color * specular * specularColor;

	specularColor *= light.intensity;
	diffuseColor *= attenuation * light.intensity;
	specularColor *= attenuation * light.intensity;

	//return vec3(lambertian, lambertian, lambertian);
	return vec3(ambientColor + diffuseColor + specularColor);
}


vec3 calculateDirLight(DirLight light, vec3 N, vec2 texCoords) {
	vec3 diffuseColor = determineBaseColor(texCoords);
	vec3 specularColor = vec3(1.0, 1.0, 1.0);
	float ambientIntensity = 0.1f;
	float shininessFactor = 16.0;

	if (hasMaterial) {
		specularColor = matSpecularColor;
		shininessFactor = shininess;
	}

	vec3 lightDir = -light.dir;
	float lambertian = max(dot(lightDir, N), 0.0);
	float specular = 0.0;

	if (lambertian > 0.0) {
		vec3 reflectDir = reflect(-lightDir, N);
		vec3 viewDir = normalize(-pos);

		vec3 halfDir = normalize(lightDir + viewDir);
		float specAngle = max(dot(halfDir, N), 0.0);
		specular = pow(specAngle, shininessFactor);
		if (hasSpecularMap) specular = specular * (texture(specularMap, texCoords).x);
	}

	vec3 ambientColor = light.color * ambientIntensity * diffuseColor;
	diffuseColor = light.color * lambertian * diffuseColor;
	specularColor = light.color * specular * specularColor;

	specularColor *= light.intensity;
	diffuseColor *= light.intensity;
	specularColor *= light.intensity;

	//return lightDir;
	//return vec3(lambertian, lambertian, lambertian);
	return vec3(ambientColor + diffuseColor + specularColor);
}



void main() {
	mat3 cotangentFrame = computeCotangentFrame(normal, uv);
	vec2 texCoords = vec2(uv.x, -uv.y);

	// currently non-functioning parallax occlusion mapping code
//	if (hasDepthMap) {
//		texCoords = parallaxOcclusionMapping(uv, cotangentFrame);
//		//texCoords = parallaxMapping(uv, cotangentFrame);
//		if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0) {
//			discard;
//		}
//	}

	vec3 N = normalize(normal);
	if (hasNormalMap) {
		N = computeNormalFromTexture(N, texCoords, cotangentFrame);
		//N = computeNormalFromTexture(worldNormal);
	}

	float alpha = 1.0;
	if (hasOpacityMap) {
		alpha = determineOpacity(texCoords);
	}

	vec3 finalColor3 = vec3(0.0, 0.0, 0.0);

	for(int i = 0; i < pointLightsCount; i++) {
		PointLight light;
		light.color = pointLightColors[i];
		light.pos = pointLightPositions[i];
		light.intensity = pointLightIntensities[i];
		light.constant = pointLightAttenuations[i].x;
		light.linear = pointLightAttenuations[i].y;
		light.quadratic = pointLightAttenuations[i].z;
		
		finalColor3 += calculatePointLight(light, N, texCoords);
	}

	for(int i = 0; i < dirLightsCount; i++) {
		DirLight light;
		light.color = dirLightColors[i];
		light.intensity = dirLightIntensities[i];
		light.dir = dirLightDirections[i];
		
		finalColor3 += calculateDirLight(light, N, texCoords);
	}

	finalColor = vec4(finalColor3, alpha);
	//finalColor = vec4(texCoords, 0.0, alpha);
}