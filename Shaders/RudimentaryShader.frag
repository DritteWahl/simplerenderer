#version 450

// UNIFORMS
uniform mat4 MVP;

// INPUT
in vec4 color;

// SHADER CODE
void main(){
	gl_FragColor = color;
}