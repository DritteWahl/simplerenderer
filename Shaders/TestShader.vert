#version 450

// UNIFORMS
uniform mat4 MVP;

// INPUT
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_color;
layout(location = 3) in vec2 in_uv;

// OUTPUT
layout(location = 0) out vec4 color;


// SHADER CODE
void main(){
	vec4 MVPpos = MVP * vec4(in_pos, 1.0);
	gl_Position = MVPpos;
	color = vec4(in_color, 1.0);
}