#version 450

// UNIFORMS
uniform mat4 MVP;

// INPUT
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_color;
layout(location = 3) in vec2 in_uv;

/*
in Vertex {
	layout(location = 0) vec3 pos;
	layout(location = 1) vec3 normal;
	layout(location = 2) vec3 color;
	layout(location = 3) vec2 uv;
}
*/

// OUTPUT
layout(location = 0) out vec4 color;


// SHADER CODE
void main(){
	vec4 pos4 = vec4(in_pos, 1.0f);
	vec4 mvpPos = MVP * pos4;
	gl_Position = mvpPos;
	color = vec4(in_color, 1.0);
}
