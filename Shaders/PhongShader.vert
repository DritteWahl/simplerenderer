#version 450

// UNIFORMS
uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform mat3 normalMatrix;

uniform int pointLightsCount;
uniform vec3[10] pointLightColors;
uniform float[10] pointLightIntensities;
uniform vec3[10] pointLightPositions;
uniform vec3[10] pointLightAttenuations;

uniform int dirLightsCount;
uniform vec3[10] dirLightColors;
uniform float[10] dirLightIntensities;
uniform vec3[10] dirLightDirections;

uniform bool hasMaterial;
uniform vec3 matDiffuseColor;
uniform vec3 matSpecularColor;
uniform float shininess;

uniform bool hasDiffuseMap = false;
uniform sampler2D diffuseMap;
uniform bool hasNormalMap = false;
uniform sampler2D normalMap;
uniform bool hasSpecularMap = false;
uniform sampler2D specularMap;
uniform bool hasOpacityMap = false;
uniform sampler2D opacityMap;
uniform bool hasDepthMap = false;
uniform sampler2D depthMap;


// INPUT
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_color;
layout(location = 3) in vec2 in_uv;

// OUTPUT
layout(location = 0) out vec3 pos;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 color;
layout(location = 3) out vec2 uv;
layout(location = 4) out vec3 worldPos;
layout(location = 5) out vec3 worldNormal;

// SHADER CODE
void main(){
	mat4 MVP = P * V * M;
	mat4 MV = V * M;
	vec4 MVPpos = MVP * vec4(in_pos, 1.0);
	gl_Position = MVPpos;

	vec4 vertexPos4 = MV * vec4(in_pos, 1.0);
	pos = vec3(vertexPos4);
	normal = normalMatrix * in_normal;
	color = in_color;
	uv = in_uv;
	worldPos = in_pos;
	worldNormal = in_normal;
	
}