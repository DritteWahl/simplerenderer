#include "SceneImporter.hpp"

#include <assimp/postprocess.h>

#include "SceneGraph/Scene.hpp"
#include "SceneGraph/TriangularMesh.hpp"
#include "SceneGraph/GeometricObject.hpp"


ShaderProgram* SceneImporter::shaderProgram = nullptr;


SceneObject* SceneImporter::importFile(const std::string& file, Scene* scene, SceneObject* parent) {
	if (shaderProgram == nullptr) {
		throw std::exception("No shader program given to SceneImporter: scene can not be imported without specifiying a shader program for them!");
	}

	Assimp::Importer importer;

	const aiScene* assimpScene = importer.ReadFile(file, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices);

	if (assimpScene == nullptr) {
		printf(importer.GetErrorString());
		return nullptr;
	}

	const aiNode* root = assimpScene->mRootNode;

	SceneObject* rootObject = processNode(root, assimpScene, scene, parent);

	return rootObject;
}


SceneObject* SceneImporter::processNode(const aiNode* node, const aiScene* assimpScene, Scene* scene, SceneObject* parent) {
	aiMatrix4x4 transformMatrix = node->mTransformation;
	glm::mat4 glmMatrix = {
		transformMatrix.a1, transformMatrix.b1, transformMatrix.c1, transformMatrix.d1,
		transformMatrix.a2, transformMatrix.b2, transformMatrix.c2, transformMatrix.d2,
		transformMatrix.a3, transformMatrix.b3, transformMatrix.c3, transformMatrix.d3,
		transformMatrix.a4, transformMatrix.b4, transformMatrix.c4, transformMatrix.d4
	};

	SceneObject* sceneObject = new SceneObject(glmMatrix);

	if (parent == nullptr) {
		scene->addSceneObject(sceneObject);
	}
	else {
		scene->addSceneObject(sceneObject, parent);
	}

	for (size_t i = 0; i < node->mNumMeshes; ++i) {
		aiMesh* assimpMesh = assimpScene->mMeshes[node->mMeshes[i]];
		GeometricObject* mesh = importMesh(assimpMesh, assimpScene);
		if (mesh == nullptr) {
			continue;
		}
		scene->addSceneObject(mesh, sceneObject);
	}

	for (size_t i = 0; i < node->mNumChildren; ++i) {
		processNode(node->mChildren[i], assimpScene, scene, sceneObject);
	}

	return sceneObject;
}

GeometricObject* SceneImporter::importMesh(const aiMesh* inMesh, const aiScene* assimpScene) {
	GeometricObject* meshObject = new GeometricObject();
	TriangularMesh* mesh = new TriangularMesh();

	meshObject->setGeometry(mesh);
	
	if (!inMesh->HasFaces() && !inMesh->HasPositions()) {
		return nullptr;
	}

	std::vector<Vertex> vertices = getVertices(inMesh);
	mesh->setVertices(vertices);

	aiMaterial* inMaterial = assimpScene->mMaterials[inMesh->mMaterialIndex];
	Material* material = importMaterial(inMaterial);
	meshObject->setMaterial(material);

	std::vector<Texture*> textures = getTextures(inMaterial);
	for (Texture* tex : textures) {
		meshObject->addTexture(tex);
	}

	meshObject->setShaderProgram(shaderProgram);
	meshObject->setIsIlluminated(true);

	return meshObject;
}

std::vector<Vertex> SceneImporter::getVertices(const aiMesh* inMesh) {
	unsigned int faceCount = inMesh->mNumFaces;
	aiFace* triangles = inMesh->mFaces;
	std::vector<Vertex> vertices;

	for (size_t i = 0; i < faceCount; ++i) {
		// check if face is actually a triangle
		if (triangles[i].mNumIndices != 3) {
			std::cout << "FACE INDICES != 3!" << std::endl;
			continue;
		}

		// process all vertices of this face
		for (int j = 0; j < 3; ++j) {
			unsigned int vertexIdx = triangles[i].mIndices[j];

			// vertex position
			aiVector3D vert = inMesh->mVertices[vertexIdx];
			glm::vec3 pos = glm::vec3(vert.x, vert.y, vert.z);

			// normal
			glm::vec3 normal = glm::vec3(0.0);
			if (inMesh->HasNormals()) {
				aiVector3D n = inMesh->mNormals[vertexIdx];
				normal = glm::vec3(n.x, n.y, n.z);
			}

			// texture coords
			glm::vec2 uv = glm::vec2(0.0);
			if (inMesh->HasTextureCoords(0)) {
				aiVector3D tex = inMesh->mTextureCoords[0][vertexIdx];
				uv = glm::vec2(tex.x, tex.y);
			}

			// vertex color
			glm::vec3 color = glm::vec3(0.0);
			if (inMesh->HasVertexColors(0)) {
				size_t arrLength = sizeof(inMesh->mColors) / sizeof(*(inMesh->mColors));
				if (arrLength > vertexIdx && inMesh->mColors[vertexIdx] != nullptr) {
					aiColor4D col = inMesh->mColors[vertexIdx][0];
					color = glm::vec3(col.r, col.g, col.b);
				}
			}

			// push back vertex
			Vertex* vertex = new Vertex(pos, normal, color, uv);
			vertices.push_back(*vertex);
		}
	}

	return vertices;
}

Material* SceneImporter::importMaterial(const aiMaterial* inMaterial) {
	aiColor3D diffuseColor;
	aiColor3D specularColor;
	float shininess;
	inMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
	inMaterial->Get(AI_MATKEY_COLOR_SPECULAR, specularColor);
	inMaterial->Get(AI_MATKEY_REFLECTIVITY, shininess);

	Material* material = new Material();
	material->setDiffuseColor(diffuseColor.r, diffuseColor.g, diffuseColor.b);
	material->setSpecularColor(specularColor.r, specularColor.g, specularColor.b);
	material->setShininess(16.0f);

	return material;
}

std::vector<Texture*> SceneImporter::getTextures(const aiMaterial* inMaterial) {
	// TODO handle more than diffuse maps
	// TODO maybe handle embedded textures?

	std::vector<Texture*> textures;
	aiString* texturePath = new aiString("");

	// diffuse map
	if (inMaterial->GetTextureCount(aiTextureType::aiTextureType_DIFFUSE) > 0) {
		inMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, texturePath);
		Texture* tex = new Texture(DiffuseMap);
		tex->setTextureImage(texturePath->C_Str());
		tex->create();
		textures.push_back(tex);
	}

	// normal map
	if (inMaterial->GetTextureCount(aiTextureType::aiTextureType_NORMALS) > 0) {
		inMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, texturePath);
		Texture* tex = new Texture(NormalMap);
		tex->setTextureImage(texturePath->C_Str());
		tex->create();
		textures.push_back(tex);
	}

	// specular map
	if (inMaterial->GetTextureCount(aiTextureType::aiTextureType_SPECULAR) > 0) {
		inMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, texturePath);
		Texture* tex = new Texture(SpecularMap);
		tex->setTextureImage(texturePath->C_Str());
		tex->create();
		textures.push_back(tex);
	}

	// opacity map
	if (inMaterial->GetTextureCount(aiTextureType::aiTextureType_OPACITY) > 0) {
		inMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, texturePath);
		Texture* tex = new Texture(OpacityMap);
		tex->setTextureImage(texturePath->C_Str());
		tex->create();
		textures.push_back(tex);
	}

	// height/depth map
	if (inMaterial->GetTextureCount(aiTextureType::aiTextureType_HEIGHT) > 0) {
		inMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, texturePath);
		Texture* tex = new Texture(DepthMap);
		tex->setTextureImage(texturePath->C_Str());
		tex->create();
		textures.push_back(tex);
	}

	//const aiTexture* tex = assimpScene->GetEmbeddedTexture(diffusePath->C_Str());
	return textures;
}

void SceneImporter::setShaderProgram(ShaderProgram* inShaderProgram) {
	shaderProgram = inShaderProgram;
}
