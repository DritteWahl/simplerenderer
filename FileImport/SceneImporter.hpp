#pragma once

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <vector>

#include "SceneGraph/ShaderProgram.hpp"

class Scene;
class SceneObject;
class GeometricObject;
class Texture;
class Material;
struct Vertex;


static class SceneImporter {

public:
	static SceneObject* importFile(const std::string& file, Scene* scene, SceneObject* parent);
	static void setShaderProgram(ShaderProgram* inShaderProgram);

private:
	static SceneObject* processNode(const aiNode* node, const aiScene* assimpScene, Scene* scene, SceneObject* parent);
	static GeometricObject* importMesh(const aiMesh* inMesh, const aiScene* assimpScene);
	static std::vector<Vertex> getVertices(const aiMesh* inMesh);
	static Material* importMaterial(const aiMaterial* inMaterial);
	static std::vector<Texture*> getTextures(const aiMaterial* inMaterial);

	static ShaderProgram* shaderProgram;

};