#pragma once

#include "Window/Window.hpp"
#include "SceneGraph/Scene.hpp"
#include "SceneGraph/Primitives/Cube.hpp"

#include <iostream>


void buildScene(Scene& scene) {
	PointLight* light = new PointLight();
	light->setColor(1.0, 1.0, 1.0);
	light->setIntensity(1.5);
	light->setAttenuation(1.0, 0.09, 0.032);
	light->translate(3.0, 3.0, 3.0);

	PointLight* light2 = new PointLight();
	light2->setColor(1.0, 1.0, 1.0);
	light2->setIntensity(1.5);
	light2->setAttenuation(1.0, 0.09, 0.032);
	light2->translate(-3.0, 3.0, -3.0);

	DirectionalLight* dirLight = new DirectionalLight();
	dirLight->setColor(1.0, 1.0, 1.0);
	dirLight->setIntensity(0.75);
	dirLight->setEulerAngles(PI * 0.5, 0, -PI * 0.5);

	DirectionalLight* dirLight2 = new DirectionalLight();
	dirLight2->setColor(1.0, 1.0, 1.0);
	dirLight2->setIntensity(1.0);
	dirLight2->setEulerAngles(-PI * 0.5, 0, -PI * 0.5);

	Material* material1 = new Material();
	material1->setDiffuseColor(1.0, 0.0, 0.0);
	material1->setSpecularColor(1.0, 1.0, 1.0);
	material1->setShininess(64.0);

	ShaderProgram* shader = new ShaderProgram();
	std::string vertShaderPath = "Shaders/PhongShader.vert";
	std::string fragShaderPath = "Shaders/PhongShader.frag";
	shader->setShaders(vertShaderPath, fragShaderPath);
	shader->create();
	shader->setSeperatedMVP(true);

	Texture* brickTex = new Texture(TextureType::DiffuseMap);
	brickTex->setTextureImage("TestData/Brick3Repeated.png");
	brickTex->create();
	Texture* brickNormalTex = new Texture(TextureType::NormalMap);
	brickNormalTex->setTextureImage("TestData/Brick3RepeatedNormal.png");
	brickNormalTex->create();
	Texture* brickDepthTex = new Texture(TextureType::DepthMap);
	brickDepthTex->setTextureImage("TestData/Brick3RepeatedDepth.png");
	brickDepthTex->create();

	Cube* cube = new Cube();
	cube->setShaderProgram(shader);
	cube->setMaterial(material1);
	cube->addTexture(brickTex);
	cube->addTexture(brickNormalTex);
	cube->addTexture(brickDepthTex);

	scene.addPointLight(light);
	scene.addPointLight(light2);
	scene.addDirectionalLight(dirLight);
	//scene.addDirectionalLight(dirLight2);
	scene.addSceneObject(cube);
}


int main(int argc, char* argv[]) {
	Window renderer(1920, 1080);

	Camera camera;
	camera.setAspectRatio(16.0, 9.0);
	camera.setZNear(0.1);
	camera.setZFar(1000.0);
	camera.setFov(0.5 * PI);
	camera.setPerspectiveProjection(true);
	camera.translateLocal(0.0, 0.0, 3.0);
	camera.rotate(PI / 4.0, PI / 4.0, 0.0);

	Scene* scene = renderer.getViewport()->getScene();

	scene->addCamera(&camera);
	scene->toggleCamera();

	buildScene(*scene);

	renderer.start();

	return 0;
}