#define CATCH_CONFIG_RUNNER
#include <catch.hpp>
#include <gl/glew.h>

int main(int argc, char* argv[]) {

	int result = Catch::Session().run(argc, argv);

	// global clean-up

	return result;
}


TEST_CASE("Catch2 testing framework works and GLContext is built up correctly", "") {
	//REQUIRE_FALSE(SDL_GL_GetCurrentContext() == NULL);-
}