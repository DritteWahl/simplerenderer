#include "CameraManager.hpp"

#include "SceneGraph/Node.hpp"

CameraManager::CameraManager() {
	
}

CameraManager::~CameraManager() {
	
}

void CameraManager::addCamera(Camera* camera) {
	cameraList.emplace_back(camera);
}

void CameraManager::removeCamera(uint id) {
	if (cameraList.empty()) {
		// TODO better exception handling	
		//std::cout << "WARNING: Camera list is empty for this viewport!";
		throw std::runtime_error("ERROR: Camera list is empty for this viewport!");
	}

	std::vector<Camera*>::iterator it = getCameraIndex(id);
	if (it == cameraList.end()) {
		// TODO better exception handling
		//std::cout << "WARNING: Camera can not be removed: Camera not found!";
		throw std::runtime_error("ERROR: Camera can not be removed : Camera not found!");
	}

	cameraList.erase(it);
}

Camera* CameraManager::getFirstCamera() {
	if (cameraList.empty()) {
		// TODO better exception handling	
		//std::cout << "WARNING: Camera list is empty for this viewport!";
		throw std::runtime_error("ERROR: Camera list is empty for this viewport!");
	}

	return cameraList[0];
}


Camera* CameraManager::getNextCamera(uint id) {
	if (cameraList.empty()) {
		// TODO better exception handling	
		//std::cout << "WARNING: Camera list is empty for this viewport!";
		throw std::runtime_error("ERROR: Camera list is empty for this viewport!");
	}

	std::vector<Camera*>::iterator it = getCameraIndex(id);
	if (it == cameraList.end()) {
		// TODO better exception handling		
		//std::cout << "WARNING: Active camera not found: Switched to first camera of the list!";
		throw std::runtime_error("ERROR: Camera list is empty for this viewport!");
		//return cameraList[0];
	}

	if (it == cameraList.end() - 1) {
		return cameraList[0];
	}

	return *(it + 1);
}

std::vector<Camera*>::iterator CameraManager::getCameraIndex(uint id) {
	std::vector<Camera*>::iterator it;
	for (it = cameraList.begin(); it != cameraList.end(); ++it) {
		if ((*it)->getNode()->getId() == id) {
			break;
		}
	}

	return it;
}
