#pragma once

#include <memory>
#include "Viewport.hpp"
#include "Tools/SDLWrapper.hpp"
#include "InputManagement/InputHandler.hpp"

class Window {
	
public:
	Window();
	Window(int width, int height);
	~Window();
	SDL_Window* getWindow();
	Viewport* getViewport();
	void update();
	void draw();
	void createViewports();
	void start();
	void pause();
	void exit();

	bool isQuit = false;
	
private:
	void initWindow(int width, int height);
	void pollEvents();
	void handleWindowEvent(SDL_Event* event);
	void run();
	size_t getFocusedViewportIndex();

	sdl2::WindowPtr window;
	std::unique_ptr<Viewport> viewport;
	std::unique_ptr<InputHandler> inputHandler;

	std::array<int, 2> resolution;
	bool isRunning = false;
	
};
