#include <SDL.h>

#pragma once

class SDLWindow {	// TODO

public:
	virtual void initWindow() = 0;
	virtual SDL_Window* getWindow() = 0;

protected:
	SDL_Window* window = nullptr;	// TODO
};
