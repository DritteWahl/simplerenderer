#pragma once

#include <glm/glm.hpp>
#include <vector>

#include "SceneGraph/DirectionalLight.hpp"

struct RenderContext {
	
	RenderContext();
	~RenderContext();

	glm::mat4 view;
	glm::mat4 projection;

	std::vector<glm::vec3> pointLightPositions;
	std::vector<glm::vec3> pointLightColors;
	std::vector<float> pointLightIntensities;
	std::vector<glm::vec3> pointLightAttenuations;

	std::vector<glm::vec3> dirLightDirections;
	std::vector<glm::vec3> dirLightColors;
	std::vector<float> dirLightIntensities;

};

