#include "Window.hpp"

#include <iostream>
#include <stdexcept>	

Window::Window() : Window(800, 600) {

}

Window::Window(int width, int height) {
	initWindow(width, height);
	int w = 0;
	int h = 0;
	SDL_GetWindowSize(window.get(), &w, &h);
	resolution = {w, h};

	createViewports();
}

Window::~Window() {
	SDL_DestroyWindow(window.get());
}

SDL_Window* Window::getWindow() {
	return window.get();
}

Viewport * Window::getViewport() {
	return viewport.get();
}

void Window::run() {
	isRunning = true;

	while (isRunning) {
		if (isQuit) {
			isRunning = false;
			exit();
			break;
		}

		update();
		draw();
	}
}

void Window::update() {
	pollEvents();
	viewport->updateScene();
}

void Window::draw() {
	viewport->drawScene();

	SDL_GL_SwapWindow(window.get());
}

void Window::createViewports() {
	viewport = std::unique_ptr<Viewport>(new Viewport(resolution[0], resolution[1], window.get()));
	inputHandler = std::unique_ptr<InputHandler>(new InputHandler(viewport.get()));
}

void Window::start() {
	run();
}

void Window::pause() {
	isRunning = false;
}

// TODO make more dynamic
void Window::initWindow(int width, int height) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		// TODO change exception type
		throw std::runtime_error("SDL Initialization failed!");
	}

	window = sdl2::WindowPtr(SDL_CreateWindow("Graphics API", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL));
	if (!window) {
		// TODO change exception type
		throw std::runtime_error("SDL Window creation failed!");
	}
}

void Window::pollEvents() {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			isQuit = true;
		}
		else if (event.type == SDL_WINDOWEVENT) {
			handleWindowEvent(&event);
		}
		else {
			inputHandler->handleEvent(&event);
		}

	}
}

void Window::handleWindowEvent(SDL_Event* event) {
	// TODO resize, close etc
}

size_t Window::getFocusedViewportIndex() {
	return 0;
}

void Window::exit() {	
	SDL_DestroyWindow(window.get());
	SDL_Quit();
}
