#pragma once

#include "Renderer.hpp"
#include "SceneGraph/Scene.hpp"

#include <array>

class Viewport {
	
public:
	Viewport(int width, int height, SDL_Window* window);
	~Viewport();

	void updateScene();
	void drawScene() const;
	void setGLContext(Renderer* newContext);
	Scene* getScene();


protected:
	Renderer* renderer = nullptr;
	Scene* activeScene = nullptr;

	std::array<int, 2> resolution;
};
