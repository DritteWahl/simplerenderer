#include "Renderer.hpp"

#include <stdexcept>
#include "../Tools/Helpers.hpp"

Renderer::Renderer(SDL_Window* contextWindow) {
	window = contextWindow;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_SetSwapInterval(1);

	context = SDL_GL_CreateContext(window);
	// TODO change exception type
	if (context == nullptr) {
		throw std::runtime_error("Context Creation failed!");
	}

	clearColor = {0.4, 0.7, 0.85, 1.0};

	setInitialState();
}

Renderer::~Renderer() {
	SDL_GL_DeleteContext(context);
}

void Renderer::makeCurrent() {
	SDL_GL_MakeCurrent(window, context);

	helpers::checkGLError();
}

void Renderer::clearBuffer() {
	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

	helpers::checkGLError();
}

void Renderer::setAlphaBlending(bool isOn) {
	if (isOn) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		alphaBlendingOn = true;
	}
	else {
		glDisable(GL_BLEND);
		alphaBlendingOn = false;
	}
}

void Renderer::toggleAlphaBlending() {
	if (alphaBlendingOn) setAlphaBlending(false);
	else setAlphaBlending(true);
}

void Renderer::setBackfaceCulling(bool isOn) {
	if (isOn) {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		backfaceCullingOn = true;
	}
	else {
		glDisable(GL_CULL_FACE);
		backfaceCullingOn = false;
	}
}

void Renderer::toggleBackfaceCulling() {
	if (backfaceCullingOn) setBackfaceCulling(false);
	else setBackfaceCulling(true);
}

void Renderer::setInitialState() {
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	alphaBlendingOn = true;

	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	backfaceCullingOn = false;

	helpers::checkGLError();

	clearBuffer();
}
