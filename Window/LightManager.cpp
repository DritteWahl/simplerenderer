#include "LightManager.hpp"

LightManager::LightManager() {
	
}

LightManager::~LightManager() {
	
}

void LightManager::addPointLight(PointLight* light) {
	pointLights.push_back(light);
}

void LightManager::addDirectionalLight(DirectionalLight* light) {
	directionalLights.push_back(light);
}

std::vector<PointLight*> LightManager::getPointLights() {
	return pointLights;
}

std::vector<DirectionalLight*> LightManager::getDirectionalLights() {
	return directionalLights;
}
