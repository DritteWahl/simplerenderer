#pragma once

#include <SDL.h>
#include <glm/vec4.hpp>

class Renderer {
	
public:
	Renderer(SDL_Window* contextWindow);
	~Renderer();
	void makeCurrent();
	void clearBuffer();

	void setAlphaBlending(bool isOn);
	void toggleAlphaBlending();
	void setBackfaceCulling(bool isOn);
	void toggleBackfaceCulling();

private:
	void setInitialState();

	SDL_GLContext context;
	SDL_Window* window;

	glm::vec4 clearColor;

	bool alphaBlendingOn;
	bool backfaceCullingOn;
};
