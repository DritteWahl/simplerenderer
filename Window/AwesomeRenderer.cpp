#include "AwesomeRenderer.hpp"


AwesomeRenderer::AwesomeRenderer() {	

}

AwesomeRenderer::~AwesomeRenderer() {
	mainWindow.reset();
}

void AwesomeRenderer::start() {
	run();
}

void AwesomeRenderer::createWindow(int width, int height) {
	mainWindow = std::unique_ptr<Window>(new Window(width, height));
}

void AwesomeRenderer::closeWindow() {
	mainWindow.reset();
	isRunning = false;
}

void AwesomeRenderer::run() {
	isRunning = true;

	while (isRunning) {
		if (mainWindow->isQuit) {
			mainWindow.reset();
			isRunning = false;
			break;
		}

		mainWindow->update();
		mainWindow->draw();
	}

	cleanup();
}

void AwesomeRenderer::cleanup() {
	SDL_Quit();
}
