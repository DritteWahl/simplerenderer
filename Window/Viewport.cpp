#include "Viewport.hpp"

#include <iostream>
#include <string>
#include <gl/glew.h>
#include "SceneGraph/Node.hpp"

Viewport::Viewport(int width, int height, SDL_Window* window) : resolution({width, height}) {
	renderer = new Renderer(window);

	glewExperimental = GL_FALSE;
	glewInit();

	activeScene = new Scene("emptyScene");

	// TEST scene
	//auto sceneGraph = test::createTestScene("LIGHTING", cameraManager, lightManager);
	//activeScene = new Scene("Test", sceneGraph);
	//activeCamera = cameraManager->getFirstCamera();
}

Viewport::~Viewport() {

}

void Viewport::updateScene() {
	activeScene->updateScene();
}

void Viewport::drawScene() const {
	renderer->makeCurrent();
	renderer->clearBuffer();

	activeScene->drawScene();
}

void Viewport::setGLContext(Renderer* newContext) {
	renderer = newContext;
}

Scene* Viewport::getScene() {
	return activeScene;
}



