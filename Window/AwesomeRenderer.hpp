#pragma once

#include "Window.hpp"

#include <memory>

#pragma once

class AwesomeRenderer {

public:
	AwesomeRenderer();
	~AwesomeRenderer();
	void start();
	void createWindow(int width, int height);
	void closeWindow();

private:
	void run();
	void cleanup();	

	std::unique_ptr<Window> mainWindow = nullptr;
	bool isRunning = false;
};
